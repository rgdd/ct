# Transparency log preliminaries

Inclusion and consistency proofs are key components of public transparency
logging.  How do they work and what do they prove?  This document provides
background on transparency logs and the associated proof techniques.

**Preliminaries.**
The reader is assumed to know the properties of a cryptographic hash function.

## The utility of transparency logs

A transparency log carries the utility of a tamper-evident append-only list.
Let's break this down one component at a time.

A _list_ is just a sequence of items:

    [ "foo", "bar", "baz" ]

That the list is _append-only_ means entries should only be added at the end:

    [ "foo", "bar", "baz", "qux" ]

That the list is _tamper-evident_ means removals cannot be prevented:

    [ "bar", "baz", "qux" ]

Modifications also cannot be prevented:

    [ "barrel", "baz", "qux" ]

It is however possible to _detect_ if the list's append-only property is
violated.  Such detectability is rooted in cryptography, and does not depend on
verifiers to observe the entire list which could be massive (e.g., [>1TiB][]).

[>1TiB]: https://letsencrypt.org/2022/05/19/database-to-app-tls.html

If `n` is the number of list items, an `O(log(n))` proof can convince a verifier
that no item was removed or modified.  It is also possible to prove that an item
is in the list; again without the verifier downloading the entire list.

As a rule of thumb, a transparency log proof for a very large list is 1-2KiB.

## Merkle trees as cryptographic foundation

The cryptographic building block that facilitates verification is called a
Merkle tree.  It is constructed by hashing all list items once, then repeatedly
hashing adjacent hashes in pairs of two until a single tree head remains.

                          .-   g:=H(e + f)   -.
                        /                      \
                       /                        \
                  e:=H(a + b)               f:=H(c + d)
                  /        \                /        \
                 /          \              /          \
             a:=H("foo")  b:=H("bar")  c:=H("baz")  d:=H("qux")
                    ^            ^            ^            ^
                    |            |            |            |
    list items:    foo          bar          baz          qux
    list index:     0            1            2            3

    Figure 1: Merkle tree for the list ["foo", "bar", "baz", "qux" ].  The plus
    operator ("+") refers to appending.  For example, "foo"+"bar" == "foobar".

Below is a less cluttered way to draw the above, assuming hashing for `a,...,g`.

    level 2:             .- g -.         <- root hash / tree head
                        /       \
    level 1:           e         f       <- interior hashes
                     /   \     /   \
    level 0:        a     b   c     d    <- leaf hashes
                    |     |   |     |
    list items:    foo   bar baz   qux
    list index:     0     1   2     3

    Figure 2: A dense drawing of the Merkle tree structure in Figure 1.  Any
    hashed node that is not a leaf (i.e., no children) is an interior node.

If a cryptographic hash function is used, it is computationally infeasible to
find an identical tree head `g` that covers a different list of the same size.
In other words, if you find two different lists of the same size that result in
an identical tree head you also managed to break the underlying hash function.

If the number of list items is not a perfect power of two (1, 2, 4, 8, 16, etc),
one convention to extend the tree with more leaf nodes is as follows:

          4 items                     5 items                    6 items

                                     .-  i  -.                 .-   l   -.
                     add fre        /         \   add thu     /           \
          .- g -.     --->      .- g -.        h   --->   .- g -.          k
         /       \             /       \       |         /       \        / \
        e         f           e         f     fre       e         f      h   j
      /   \     /   \       /   \     /   \           /   \     /   \    |   |
     a     b   c     d     a     b   c     d         a     b   c     d  fre thu
     |     |   |     |     |     |   |     |         |     |   |     |
    foo   bar baz   qux   foo   bar  baz   qux      foo   bar baz   qux

    Figure 3: Adding "fred" with list size 4, and "thu" with list size 5.

This is not the only way to extend a Merkle tree.  It is however the most common
approach in practise, following from [RFC 6962][] and [RFC 9162][].  For an
alternative convention, see the work on [history trees][] by Crosby and Wallach.

[RFC 6962]: https://datatracker.ietf.org/doc/html/rfc6962
[RFC 9162]: https://datatracker.ietf.org/doc/html/rfc9162
[history trees]: https://static.usenix.org/event/sec09/tech/full_papers/crosby.pdf

A log's tree head is usually signed by the operator together with metadata like
the number of list items and the current time.  Both inclusion and consistency
proofs are verified against a log's signed tree heads.  It is assumed that
everyone observes the same signed tree heads, e.g., via a [gossip protocol][].

[gossip protocol]: https://datatracker.ietf.org/doc/html/draft-ietf-trans-gossip-05

## Inclusion proofs

An inclusion proof consists of **the minimal number of tree hashes a prover
needs to prove that an item is located at a given list index**.  A verifier
accepts the proof as valid if it can be used to reconstruct a reference tree
head given only the item, list index, list size, and revealed hashes.  The proof
itself doesn't say if the reference tree head is trustworthy or not; just that a
set of inputs produce the reference tree head.  This is similar to checking the
checksum of a file: it is easy to verify that it matches, but it doesn't really
mean anything unless there is a good checksum to compare against.

The procedure to generate an inclusion proof is as follows.  Traverse the tree
starting from the root.  Aim to end the traversal at an item's leaf.  At each
level down the tree, add the sibling hash to the front of a list.

Below is an example for the list item `bar` at index `1` in a tree of size `4`:

          .- g -.       proof = []       # Start from the root
         /       \      proof = []       # Go down towards b via e
        e         f     proof = [ f ]    # At e, grab sibling f
      /   \     /   \   proof = [ f ]    # Go down to b
     a     b   c     d  proof = [ a, f ] # At b, grab sibling a
     |     |   |     |                   # Done, output proof
    foo   bar baz   qux

    Figure 4: Generating an inclusion proof for item "bar" at index 1.

The proof is verified by computing a tree head from the bottom and up, only
knowing the item `bar`, list index `1`, list size `4`, and revealed hashes
`[ a:=H("foo"), f:=H(H("baz") + H("qux")) ]`.  In other words, the verifier
does the following starting from `bar` and working up towards the root:

         .- g -.    proof = []       # Done, compare g to tree head
        /       \   proof = []       # Hash e+f to get g
       e         f  proof = []       # Select hash-f from proof
     /   \          proof = [ f ]    # Hash a+b to get e
    a     b         proof = [ f ]    # Select hash-a from proof
          |         proof = [ a, f ] # Compute b as hash of bar
         bar        proof = [ a, f ] # Start from bar at index 1

    Figure 5: Recomputing a tree head from an inclusion proof.

The verifier requires that the computed `g` matches a reference tree head, and
that the proof has the correct number of hashes which follow from the list size.

## Consistency proofs

If one or more items were included at certain list indices yesterday, it is
possible to check that they are still there today using a consistency proof.  A
consistency proof consists of **the minimum number of tree hashes a prover needs
to prove that the old list is identical to the start of a new list**.  A
verifier accepts the proof if it can be used to reconstruct the old tree head
from a subset of hashes that also reconstruct the new tree head.  The proof
itself doesn't say if the new tree head is trustworthy or not, but it is
nevertheless an append-only version of the old tree.  (In other words, for a
complete story tree heads need to be signed and gossiped sufficiently.)

The procedure to generate a consistency proof is as follows.  Traverse the new
tree starting from the root.  Aim towards the last leaf in the old tree, but
stop as soon as a subtree is reached that doesn't cover any of the appended
leaves.  Add each sibling hash to the front of a list.  Additionally add the
final subtree hash to the list if the old tree is not a perfect binary tree.

Below is an example from an old list size `3` to a new list size `6`.

              .-   l   -.      proof = []             # Start from the root; covers appended d,h,j
             /           \     proof = []             # Go down towards c via g
         .- g -.          k    proof = [ k ]          # Covers appended appended d, grab sibling k
        /       \        / \   proof = [ k ]          # Go down towards c via f
       e         f      h   j  proof = [ e, k ]       # Covers appended d, grab sibling e
     /   \     /   \           proof = [ e, k ]       # Go down to c
    a     b   c     d          proof = [ c, d, e, k ] # Does not cover appended leaves; grab d and then c
                               proof = [ c, d, e, k ] # Done, output proof

    Figure 6: Generate a consistency proof from size 3 to 6.  The old tree is
    not a perfect binary tree.  So, the "stop subtree hash" c is needed too.

Look carefully at the selected hashes and notice that every hash which
corresponds to a left child in the new tree is also a fixed subtree hash in the
old tree.  A verifier can use these hashes to reconstruct the old tree head `x`:

         Old tree           Recompute x from proof

         .- x -.    |    .- x -.     proof = [ d, k ]       # Done, compare x to old tree hash
        /       \   |   /       \    proof = [ d, k ]       # Hash e+c to get x
       e         c  |  e         c   proof = [ d, k ]       # Select hash-e and hash-c from proof
     /   \          |                proof = [ c, d, e, k ] # Start
    a     b

    Figure 7: Recomputing the old tree head from a consistency proof.  A
    verifier knows which hashes to pick as list-sizes dictate tree structures.

Using _the same nodes_ `e` and `c` which fixate the old tree, it is possible to
apply them with the proof's right-child hashes that _only cover appended items_:

                Old tree                   Recompute l from proof

              .-   l   -.      |         .-   l   -.    proof = []             # Done, compare l to new tree head
             /           \     |        /           \   proof = []             # Hash g+k to get l
         .- g -.          k    |    .- g -.          k  proof = [ k ]          # Select hash-k from proof
        /       \        / \   |   /       \            proof = [ k ]          # Hash e+f to get g
       e         f      h   j  |  e         f           proof = [ e ]          # Select hash-e from proof
     /   \     /   \           |          /   \         proof = [ e, k ]       # Hash c+d to get f
    a     b   c     d          |         c     d        proof = [ e, k ]       # Select hash-c and hash-d from proof
                                                        proof = [ c, d, e, k ] # Start

    Figure 8: Recomputing the new tree head from a consistency proof.  A
    verifier knows which hashes to pick as list-sizes dictate tree structures.

This convinces a verifier that the new tree of size `6` is an append-only
version of the old tree of size `3` because finding `e+c` that hash to two
different size-3 lists is hard, and the expected new root `l` was obtained when
also making use of right-node subtree hashes that covered the appended items.

Please note that the consistency proof generation algorithm _does not add the
hash of the final subtree if the old tree is perfect_, i.e., its underlying list
size is a perfect power of two.  This is because that subtree hash would be
identical to the old tree head and thus be redundant to a verifier.
