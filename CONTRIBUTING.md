# Contributing

## Report issues

Please report issues via [GitLab][].  You can either [create an account][], ask
for help in `#certificate-transparency` at OFTC.net and [Matrix][], or send an
email to `git+rgdd-ct-1646-issue-@gitlab.torproject.org`.

[GitLab]: https://gitlab.torproject.org/rgdd/ct
[create an account]: https://gitlab.onionize.space/
[Matrix]: https://app.element.io/#/room/#certificate-transparency:matrix.org

Issues reported via email are hidden until a maintainer makes them public.  The
email subject will be used as the issue's title, and the sender's email address
will become public in the project's [issue tracker][].  Note that you can engage
in the following issue thread by responding to the received notifications.

[issue tracker]: https://gitlab.torproject.org/rgdd/ct/-/issues

## Submit merge requests

Contributions in the form of merge requests are most welcome!  For example, you
may be fixing typos, resolve bugs, improve test coverage, add features, extend
documentation, and so forth.  For non-trivial changes, please reference an existing issue that describes
what is being fixed, opening one before submitting the merge request if needed.

Further:

  - Aim for one logical change per commit or merge request
  - Write [well-formed commit messages][]
  - Add unit tests or motivate why they are omitted
  - Document public types, functions, and constants
  - Test using `go test ./...`  in the project's root directory
  - Vet using `go vet ./...` in the project's root directory
  - Format using `go fmt ./...` in the project's root directory
  - Check that your name is in the [AUTHORS](./AUTHORS) file
  - Ensure that there are no [LICENCE](./LICENCE) conflicts

[well-formed commit messages]: https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html

The merge request's "Assignee" is the one responsible for assigning a
"Reviewer", as well as clicking the merge button after approval.  The party
submitting the merge request is responsible for addressing review comments.

Other than following the above guidelines there are no ambitions for keeping the
git history clean with excessive squashing and force pushing.  For example, it
is fine to merge "Address feedback ..." as long as the commit is still
well-formed.  That said, force-push or squash-on-merged is not frowned upon as
long as it has a purpose and is deemed a good use of affected people's time.

The current maintainers that can take on the "Assignee" role are as follows:

  | Name            | IRC        | Email                     |
  | --------------- | ---------- | ------------------------- |
  | Rasmus Dahlberg | rgdd       | rasmus (at) rgdd.se       |
  | Grégoire Détrez | Grgoire[m] | gregoire (at) mullvad.net |

## Other

If you prefer to contribute without GitLab or in private, reach out as you see
fit and we will try to agree on a solution that works for all involved parts.
