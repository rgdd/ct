package metadata

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"gitlab.torproject.org/rgdd/ct/internal/tf"
)

func TestMetadataUnmarshalMarshal(t *testing.T) {
	m := Metadata{Version{12, 34}, time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC), []Operator{}}
	for _, table := range []struct {
		desc  string
		input string
		want  Metadata
	}{
		{"invalid: json (no major)", `{"version":".34","log_list_timestamp":"2020-01-01T00:00:00Z","operators":[]}`, Metadata{}},
		{"invalid: parse (no operators)", `{"version":"12.34","log_list_timestamp":"2020-01-01T00:00:00Z"}`, Metadata{}},
		{"valid", `{"version":"12.34","log_list_timestamp":"2020-01-01T00:00:00Z","operators":[]}`, m},
	} {
		var got Metadata
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	_, err := (&Metadata{Version{12, 34}, time.Time{}, []Operator{}}).MarshalJSON()
	tf.Error(t, "invalid: no timestamp", err)
}

func TestVersionUnmarshalMarshal(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  Version
	}{
		{"invalid: json (missing quote)", `12.34"`, Version{}},
		{"invalid: parse (extra dot)", `"1.2.34"`, Version{}},
		{"valid", `"12.34"`, Version{12, 34}},
	} {
		var got Version
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
}

func TestOperatorUnmarshalMarshal(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  Operator
	}{
		{"invalid: json (empty email)", `{"name":"foo","email":[""],"logs":[]}`, Operator{}},
		{"invalid: parse (no logs)", `{"name":"foo","email":["a@foo.org"]}`, Operator{}},
		{"valid", `{"name":"foo","email":["a@foo.org"],"logs":[]}`, Operator{"foo", []Email{"a@foo.org"}, []Log{}}},
	} {
		var got Operator
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	_, err := (&Operator{"foo", []Email{}, []Log{}}).MarshalJSON()
	tf.Error(t, "invalid: no email", err)
}

func TestEmailUnmarshalMarshal(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  Email
	}{
		{"invalid: json (missing quote)", `foo@example.org"`, ""},
		{"invalid: check (not an email)", `""`, ""},
		{"valid", `"foo@example.org"`, "foo@example.org"},
	} {
		var got Email
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	email := Email("")
	_, err := email.MarshalJSON()
	tf.Error(t, "invalid: not an email", err)
}

func TestLogUnmarshalMarshal(t *testing.T) {
	id, der, pub, _ := tf.ECDSA(t)
	url := LogURL("https://example.org/")
	s := fmt.Sprintf("{\"key\":\"%s\",\"log_id\":\"%s\",\"url\":\"%s\"}", tf.B64(t, der), tf.B64(t, id[:]), url)
	for _, table := range []struct {
		desc  string
		input string
		want  Log
	}{
		{"invalid: json (unknown type)", s[:len(s)-1] + `,"log_type":"foo"}`, Log{}},
		{"invalid: parse (no url)", s[:strings.Index(s, "\"url")-1] + "}", Log{}},
		{"valid", s, Log{Key: LogKey{Public: pub}, MMD: defaultLogMMD, URL: url}},
	} {
		var got Log
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	_, err := (&Log{URL: url}).MarshalJSON()
	tf.Error(t, "invalid: no key", err)
}

func TestLogKeyUnmarshalMarshal(t *testing.T) {
	_, der, pub, _ := tf.ECDSA(t)
	_, fail, _, _ := tf.Ed25519(t)
	for _, table := range []struct {
		desc  string
		input string
		want  LogKey
	}{
		{"invalid: json (too many quotes)", fmt.Sprintf("\"\"%s\"\"", tf.B64(t, der)), LogKey{}},
		{"invalid: parse (unknown key type)", fmt.Sprintf("\"%s\"", tf.B64(t, fail)), LogKey{}},
		{"valid", fmt.Sprintf("\"%s\"", tf.B64(t, der)), LogKey{pub}},
	} {
		var got LogKey
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	_, err := (&LogKey{fail}).MarshalJSON()
	tf.Error(t, "invalid: unknown key type", err)
}

func TestLogMMDUnmarshalMarshal(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  LogMMD
	}{
		{"invalid: json (not a number)", `01`, LogMMD(0)},
		{"invalid: check (zero value)", `0`, LogMMD(0)},
		{"valid", `1`, LogMMD(1)},
	} {
		var got LogMMD
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	mmd := LogMMD(0)
	_, err := mmd.MarshalJSON()
	tf.Error(t, "invalid: zero", err)
}

func TestLogURLUnmarshalMarshal(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  LogURL
	}{
		{"invalid: json (missing quote)", `https://example.org/"`, ""},
		{"invalid: check (not a url)", `""`, ""},
		{"valid", `"https://example.org/"`, "https://example.org/"},
	} {
		var got LogURL
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	url := LogURL("")
	_, err := url.MarshalJSON()
	tf.Error(t, "invalid: not a base url", err)
}

func TestLogDNSNameUnmarshalMarshal(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  LogDNSName
	}{
		{"invalid: json (mising quote)", `example.org"`, "example.org"},
		{"invalid: check (not a hostname)", `""`, "example.org"},
		{"valid", `"example.org"`, "example.org"},
	} {
		var got LogDNSName
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	name := LogDNSName("")
	_, err := name.MarshalJSON()
	tf.Error(t, "invalid: not a hostname", err)
}

func TestLogShardUnmarshalMarshal(t *testing.T) {
	start := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	end := time.Date(2020, 12, 31, 0, 0, 0, 0, time.UTC)
	for _, table := range []struct {
		desc  string
		input string
		want  LogShard
	}{
		{"invalid: json (small-t)", `{"start_inclusive:"2020-01-01t00:00:00Z","end_exclusive":"2020-12-31T00:00:00Z"}`, LogShard{}},
		{"invalid: parse (no start)", `{"end_exclusive":"2020-12-31T00:00:00Z"}`, LogShard{}},
		{"valid", `{"start_inclusive":"2020-01-01T00:00:00Z","end_exclusive":"2020-12-31T00:00:00Z"}`, LogShard{start, end}},
	} {
		var got LogShard
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	_, err := (&LogShard{time.Time{}, time.Now()}).MarshalJSON()
	tf.Error(t, "invalid: zero start", err)
}

func TestLogTypeUnmarshalMarshal(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  LogType
	}{
		{"invalid: json (missing quote)", `test"`, LogType("")},
		{"invalid: check (unknown type)", `"foo"`, LogType("")},
		{"valid", `"prod"`, LogTypeProd},
	} {
		var got LogType
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	typ3 := LogType("")
	_, err := typ3.MarshalJSON()
	tf.Error(t, "invalid: unknown type", err)
}

func TestLogStateUnmarshalMarshal(t *testing.T) {
	d := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	for _, table := range []struct {
		desc  string
		input string
		want  LogState
	}{
		{"invalid: json (missing curly brace)", `{"pending":"timestamp":"2020-01-0T00:00:00Z"}}`, LogState{}},
		{"invalid: parse (no timestamp)", `{{"pending":{}}}`, LogState{}},
		{"valid", `{"pending":{"timestamp":"2020-01-01T00:00:00Z"}}`, LogState{Name: LogStatePending, EnteredAt: d}},
	} {
		var got LogState
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	_, err := (&LogState{Name: LogStateName(0), EnteredAt: time.Now()}).MarshalJSON()
	tf.Error(t, "invalid: unknown state", err)
}

func TestPreviousOperatorUnmarshalMarshal(t *testing.T) {
	d := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	for _, table := range []struct {
		desc  string
		input string
		want  PreviousOperator
	}{
		{"invalid: json (small-t)", `"{name":"foo","end_time":"2020-01-01t00:00:00Z"}`, PreviousOperator{}},
		{"invalid: parse (missing name)", `{"end_time":"2020-01-01T00:00:00Z"}`, PreviousOperator{}},
		{"valid", `{"name":"foo","end_time":"2020-01-01T00:00:00Z"}`, PreviousOperator{"foo", d}},
	} {
		var got PreviousOperator
		if tf.Error(t, table.desc, got.UnmarshalJSON([]byte(table.input))) {
			continue
		}
		if !tf.Equal(t, table.desc, got, table.want) {
			continue
		}

		_, err := got.MarshalJSON()
		tf.Error(t, table.desc, err)
	}
	_, err := (&PreviousOperator{"foo", time.Time{}}).MarshalJSON()
	tf.Error(t, "invalid: zero end", err)
}
