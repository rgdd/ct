package metadata

import (
	"context"
	"crypto"
	"fmt"
	"net/http"
	"testing"

	"gitlab.torproject.org/rgdd/ct/internal/ctio"
	"gitlab.torproject.org/rgdd/ct/internal/tf"
)

func TestErrLoadError(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input ErrLoad
		want  string
	}{
		{"err read meta", ErrLoad{}, "read: metadata: <nil>"},
		{"err read sig", ErrLoad{ReadMetadata: true}, "read: signature: <nil>"},
		{"err verify", ErrLoad{ReadMetadata: true, ReadSignature: true}, "verify: <nil>"},
		{"err unmarshal", ErrLoad{ReadMetadata: true, ReadSignature: true, VerifiedSignature: true}, "unmarshal: <nil>"},
	} {
		tf.Equal(t, table.desc, table.input.Error(), table.want)
	}
}

func TestNewHTTPSource(t *testing.T) {
	googlePK, googleMURL, googleSURL := ctio.MetadataParamsGoogle()
	opts := HTTPSourceOptions{
		PublicKey:    struct{}{},
		MetadataURL:  "http://example.org/metadata.json",
		SignatureURL: "http://example.org/metadata.sig",
		UserAgent:    "(redacted)",
		Timeout:      ctio.HTTPTimeout + 1,
		Client:       http.Client{Timeout: ctio.HTTPTimeout + 1},
	}
	for _, table := range []struct {
		desc  string
		input HTTPSourceOptions
		want  HTTPSourceOptions
	}{
		{"non-default", opts, opts},
		{"default", HTTPSourceOptions{}, HTTPSourceOptions{UserAgent: ctio.HTTPUserAgent, Timeout: ctio.HTTPTimeout}},
		{"google", HTTPSourceOptions{Name: "google"}, HTTPSourceOptions{
			Name:         "google",
			PublicKey:    googlePK,
			MetadataURL:  googleMURL,
			SignatureURL: googleSURL,
			UserAgent:    ctio.HTTPUserAgent,
			Timeout:      ctio.HTTPTimeout,
		}},
	} {
		tf.Equal(t, table.desc, NewHTTPSource(table.input).opts, table.want)
	}
}

func TestHTTPSourceLoad(t *testing.T) {
	ctx := context.Background()
	_, _, pub, priv := tf.Ed25519(t)
	_, m := testMetadata(t)
	msg, err := m.MarshalJSON()
	if err != nil {
		t.Fatal(err)
	}
	msgSig := tf.Ed25519Sign(t, priv, msg)

	foo := []byte("foo")
	fooSig := tf.Ed25519Sign(t, priv, foo)
	for _, table := range []struct {
		desc          string
		transport     tf.Transport
		wantMessage   []byte
		wantSignature []byte
		wantMetadata  Metadata
		wantErr       error
	}{
		{
			"invalid: read metadata",
			tf.NewTransport(t, [][]byte{nil}, []int{404}),
			nil, nil, Metadata{}, ErrLoad{false, false, false, nil, nil, fmt.Errorf("")},
		},
		{
			"invalid: read signature",
			tf.NewTransport(t, [][]byte{msg, nil}, []int{200, 404}),
			nil, nil, Metadata{}, ErrLoad{true, false, false, msg, nil, fmt.Errorf("")},
		},
		{
			"invalid: signature",
			tf.NewTransport(t, [][]byte{msg, fooSig}, []int{200, 200}),
			nil, nil, Metadata{}, ErrLoad{true, true, false, msg, fooSig, fmt.Errorf("")},
		},
		{
			"invalid: unmarshal",
			tf.NewTransport(t, [][]byte{foo, fooSig}, []int{200, 200}),
			nil, nil, Metadata{}, ErrLoad{true, true, true, foo, fooSig, fmt.Errorf("")},
		},
		{
			"valid",
			tf.NewTransport(t, [][]byte{msg, msgSig}, []int{200, 200}),
			msg, msgSig, m, nil,
		},
	} {
		s := NewHTTPSource(HTTPSourceOptions{PublicKey: pub, Client: http.Client{Transport: table.transport}})
		gotMessage, gotSignature, gotMetadata, err := s.Load(ctx)
		if err != nil {
			gotErr := err.(ErrLoad)
			wantErr := table.wantErr.(ErrLoad)
			tf.Equal(t, table.desc, gotErr.ReadMetadata, wantErr.ReadMetadata)
			tf.Equal(t, table.desc, gotErr.ReadSignature, wantErr.ReadSignature)
			tf.Equal(t, table.desc, gotErr.VerifiedSignature, wantErr.VerifiedSignature)
			tf.Equal(t, table.desc, gotErr.Message, wantErr.Message)
			tf.Equal(t, table.desc, gotErr.Signature, wantErr.Signature)
		} else {
			tf.Equal(t, table.desc, err, table.wantErr)
		}

		tf.Equal(t, table.desc, gotMessage, table.wantMessage)
		tf.Equal(t, table.desc, gotSignature, table.wantSignature)
		tf.Equal(t, table.desc, gotMetadata, table.wantMetadata)
		table.transport.Check()

		// Note the above doesn't just continue on error so that we can
		// assert there's no useful output unless err is nil.  This
		// forces library users to explicitly do hacks with ErrLoad.
	}
}

func TestHTTPSourceGet(t *testing.T) {
	ctx := context.Background()
	for _, table := range []struct {
		desc      string
		url       string
		transport tf.Transport
		want      []byte
	}{
		{"invalid: url", "http://example.org\x10", tf.NewTransport(t, [][]byte{[]byte("msg")}, []int{200}), nil},
		{"invalid: do", "http://example.org", tf.NewTransport(t, nil, nil), nil},
		{"invalid: status", "http://example.org", tf.NewTransport(t, [][]byte{nil}, []int{404}), nil},
		{"valid", "http://example.org", tf.NewTransport(t, [][]byte{[]byte("msg")}, []int{200}), []byte("msg")},
	} {
		s := NewHTTPSource(HTTPSourceOptions{Client: http.Client{Transport: table.transport}})
		got, err := s.get(ctx, table.url)
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
		table.transport.Check()
	}
}

func TestHTTPSourceVerify(t *testing.T) {
	_, _, rsaPub, rsaPriv := tf.RSA(t, 2048)
	_, _, ed25519Pub, ed25519Priv := tf.Ed25519(t)
	for _, table := range []struct {
		desc string
		pub  crypto.PublicKey
		msg  []byte
		sig  []byte
	}{
		{"invalid: key type", struct{}{}, nil, nil},
		// ed25519
		{"invalid: ed25519", ed25519Pub, []byte("msg"), tf.Ed25519Sign(t, ed25519Priv, []byte("other"))},
		{"valid: ed25519", ed25519Pub, []byte("msg"), tf.Ed25519Sign(t, ed25519Priv, []byte("msg"))},
		// rsa
		{"invalid: rsa", rsaPub, []byte("msg"), tf.RSASign(t, rsaPriv, []byte("other"))},
		{"valid: rsa", rsaPub, []byte("msg"), tf.RSASign(t, rsaPriv, []byte("msg"))},
	} {
		tf.Error(t, table.desc, verify(table.pub, table.msg, table.sig))
	}
}
