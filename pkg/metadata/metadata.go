// Package metadata provides certificate transparency log and operator metadata.
// The used format is based on Google's v3 schema and related documentation:
//
//   - https://www.gstatic.com/ct/log_list/v3/log_list_schema.json
//   - https://json-schema.org/specification-links.html#draft-7
//   - https://groups.google.com/a/chromium.org/g/ct-policy/c/IdbrdAcDQto
//   - https://googlechrome.github.io/CertificateTransparency/
//   - https://www.rfc-editor.org/rfc/rfc6962
//   - https://datatracker.ietf.org/doc/html/rfc9162#name-log-parameters
//
// The goal is to have exported types that are easy to (un)marshal correctly,
// notably including not having to do further parsing after unmarshalling.  The
// enforced value contraints are thoroughly documented in each type description.
package metadata

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"fmt"
	"regexp"
	"time"
)

// Metadata is a versioned and timestamped metadata object that contains
// information about operators and their certificate transparency logs.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - CreatedAt is non-zero on "date-time" JSON Schema format (Draft-07)
//   - Operators are required on the wire (but may be [])
//   - Operator names are unique (criteria added by this package)
//   - Log public keys are unique (RFC 9162, §4.1)
//   - Log URLs are unique (criteria added by this package)
//   - All nested types are valid, see separate type documentation
type Metadata struct {
	Version   Version
	CreatedAt time.Time
	Operators []Operator

	// Note that version and timestamp are required inspite of the schema
	// definition, see linked ct-policy email in the package description.
	// And that the schema version examples contain erratas, e.g., "1.0.0".
}

func (m *Metadata) check() error {
	if m.CreatedAt.IsZero() {
		return fmt.Errorf("metadata: log_list_timestamp: is zero")
	}

	dupName := make(map[string]bool)
	dupID := make(map[[sha256.Size]byte]bool)
	dupURL := make(map[LogURL]bool)
	for i, operator := range m.Operators {
		if dupName[operator.Name] {
			return fmt.Errorf("metadata: operators: index %d: duplicate name %q", i, operator.Name)
		}
		dupName[operator.Name] = true
		for j, log := range operator.Logs {
			if err := log.unique(dupID, dupURL); err != nil {
				return fmt.Errorf("metadata: operators: index %d: logs: index %d: %v", i, j, err)
			}
		}
	}
	return nil
}

// Version implements the "version" string.  Note that this is the metadata
// content version, i.e., not the version of the metadata format which is v3.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Formatted as <major>.<minor>, where <major> and <minor> are >= zero
//     (https://groups.google.com/a/chromium.org/g/ct-policy/c/IdbrdAcDQto)
//
// The current implementation parses <major> and <minor> with a strconv.  This
// is not the same parser as the JSON type "number" (TODO: fixme).
type Version struct {
	Major uint64
	Minor uint64
}

// Operator is an item in the "operators" array.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Name is required on the wire (but may be "")
//   - At least one email address is provided
//   - There are no duplicate email addresses
//   - Logs are required on the wire (but may be [])
//   - Log public keys are unique (RFC 9162, §4.1)
//   - Log URLs are unique (criteria added by this package)
//   - All nested types are valid, see separate type documentation
type Operator struct {
	Name   string
	Emails []Email
	Logs   []Log
}

func (o *Operator) check() error {
	if len(o.Emails) == 0 {
		return fmt.Errorf("operator: emails: empty list")
	}
	dupEmail := make(map[Email]bool)
	for i, email := range o.Emails {
		if dupEmail[email] {
			return fmt.Errorf("operator: emails: index %d: duplicate %q", i, string(email))
		}
		dupEmail[email] = true
	}
	dupID := make(map[[sha256.Size]byte]bool)
	dupURL := make(map[LogURL]bool)
	for i, log := range o.Logs {
		if err := log.unique(dupID, dupURL); err != nil {
			return fmt.Errorf("logs: index %d: %v", i, err)
		}
	}
	return nil
}

// Email is an item in the "email" array.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Valid according to the JSON Schema "email" format (Draft-07).  TODO: fixme,
//     the current implementation only checks that there's at least one "@".
type Email string

func (e *Email) check() error {
	pattern := "^.*@.*$" // TODO: https://datatracker.ietf.org/doc/html/draft-handrews-json-schema-validation-01#section-7.3.2
	ok, err := regexp.MatchString(pattern, string(*e))
	if err != nil {
		panic(fmt.Sprintf("BUG: %v", err))
	}
	if !ok {
		return fmt.Errorf("email: %q is malformed", string(*e))
	}
	return nil
}

// Log is an item in the "logs" array.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Log ID is a hash of the public key (RFC 6962, §3.2)
//   - Previous operator items are unique
//   - All nested types are valid if present, see separate type documentation
type Log struct {
	Key LogKey
	MMD LogMMD
	URL LogURL

	// Optional
	Description *string
	Type        *LogType
	Shard       *LogShard
	State       *LogState
	History     *[]PreviousOperator
	DNS         *LogDNSName
}

func (l *Log) check() error {
	if _, err := l.Key.ID(); err != nil {
		return fmt.Errorf("log: key: %v", err)
	}
	dup := make(map[[sha256.Size]byte]bool)
	if l.History != nil {
		for i, o := range *l.History {
			fpr := sha256.Sum256([]byte(fmt.Sprintf("%s:%s", o.Name, o.End)))
			if dup[fpr] {
				return fmt.Errorf("previous_operators: index %d: duplicate %+v", i, o)
			}
			dup[fpr] = true
		}
	}
	return nil
}

func (l *Log) unique(mid map[[sha256.Size]byte]bool, murl map[LogURL]bool) error {
	id, err := l.Key.ID()
	if err != nil {
		return fmt.Errorf("key: %v", err)
	}
	if mid[id] {
		return fmt.Errorf("duplicate key %q", base64.StdEncoding.EncodeToString(id[:]))
	}
	mid[id] = true
	if murl[l.URL] {
		return fmt.Errorf("duplicate url %q", string(l.URL))
	}
	murl[l.URL] = true
	return nil
}

// LogKey implements the the "key" string.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Base64-encoded DER-encoded SubjectPublicKeyInfo (RFC 5280, §4.1.2.7)
//   - Public key is ECDSA P-256 or RSA >=2048 bits (RFC 6962, §2.1.4)
type LogKey struct {
	Public crypto.PublicKey
}

// ID outputs a key ID which is also the log's ID (RFC 6962, §3.2)
func (k *LogKey) ID() ([sha256.Size]byte, error) {
	der, err := x509.MarshalPKIXPublicKey(k.Public)
	if err != nil {
		return [sha256.Size]byte{}, fmt.Errorf("key: %v", err)
	}
	return sha256.Sum256(der), k.check()
}

func (k *LogKey) check() error {
	if k.Public == nil {
		return fmt.Errorf("key: nil")
	}
	switch t := k.Public.(type) {
	case *ecdsa.PublicKey:
		pub := k.Public.(*ecdsa.PublicKey)
		if curve := elliptic.P256(); pub.Curve != curve {
			return fmt.Errorf("key: ecdsa: not p256")
		}
	case *rsa.PublicKey:
		pub := k.Public.(*rsa.PublicKey)
		if pub.N.BitLen() < 2048 {
			return fmt.Errorf("key: rsa: too few bits")
		}
	default:
		return fmt.Errorf("key: %T is not supported", t)
	}
	return nil
}

// LogMMD implements the "mmd" number.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Number is larger than zero
//   - If no "mmd" key is provided, default to 24h
type LogMMD uint64

const defaultLogMMD = LogMMD(86400) // 24h

func (m *LogMMD) check() error {
	if *m == 0 {
		return fmt.Errorf("mmd: is non-zero")
	}
	return nil
}

// LogURL implements the "url" string.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Valid according to the JSON Schema "uri" format (Draft-07)
//   - Is a base URL (a good definition is available in RFC 9162, §4.1)
//
// The current implementation only enforces that the specified base URL is on
// the format "https://<str>/" for any non-empty <str> (TODO: fixme).  A proper
// implementation should require that <str> is a server prefix on the format
// "<hostname>[:<port>][<path>]".  Further details can be found in RFC 6962.
type LogURL string

func (u *LogURL) check() error {
	pattern := "^https://.+/$" // TODO: replace ".+" with <server prefix>
	ok, err := regexp.MatchString(pattern, string(*u))
	if err != nil {
		panic(fmt.Sprintf("BUG: %v", err))
	}
	if !ok {
		return fmt.Errorf("url: %q is malformed", *u)
	}
	return nil
}

// LogDNSName implements the "dns" string.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Valid according to the JSON Schema "hostname" format (Draft-07).  TODO: fixme,
//     the current implementation only enforces that the string is non-empty.
type LogDNSName string

func (n *LogDNSName) check() error {
	pattern := "^.+$" // TODO: https://datatracker.ietf.org/doc/html/draft-handrews-json-schema-validation-01#section-7.3.3
	ok, err := regexp.MatchString(pattern, string(*n))
	if err != nil {
		panic(fmt.Sprintf("BUG: %v", err))
	}
	if !ok {
		return fmt.Errorf("%q is malformed", string(*n))
	}
	return nil
}

// LogShard implements the "temporal_interval" object.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Start is non-zero on "date-time" JSON Schema format (Draft-07)
//   - End is non-zero on "date-time" JSON Schema format (Draft-07)
//   - Start must be before end because the expressed interval is [start, end)
type LogShard struct {
	Start time.Time
	End   time.Time
}

func (s *LogShard) check() error {
	if s.Start.IsZero() {
		return fmt.Errorf("temporal_interval: start_inclusive: is zero")
	}
	if !s.Start.Before(s.End) {
		return fmt.Errorf("temporal_interval: start_inclusive: not before end_exclusive")
	}
	return nil
}

// LogType implements the "log_type" string.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Is a valid string enumeration
type LogType string

// The supported log types are test and production
const (
	LogTypeTest LogType = "test"
	LogTypeProd LogType = "prod"
)

func (t *LogType) check() error {
	switch *t {
	case LogTypeTest:
		return nil
	case LogTypeProd:
		return nil
	default:
		return fmt.Errorf("log_type: %q is not supported", *t)
	}
}

// LogStateName is the name of a log state in the Google Chrome life cycle, see:
// https://googlechrome.github.io/CertificateTransparency/log_states.html
type LogStateName int

// Of note is that the pending state can reach all other states; the qualified
// state can reach all but the pending state; the usable state can reach all but
// the pending and qualified states; ...; and the rejected state is a sink.
const (
	LogStatePending LogStateName = iota + 1
	LogStateQualified
	LogStateUsable
	LogStateReadOnly
	LogStateRetired
	LogStateRejected
)

// ValidFrom checks if a named state is a valid transition from another one
func (n *LogStateName) ValidFrom(another LogStateName) error {
	if err := another.check(); err != nil {
		return fmt.Errorf("from: %v", err)
	}
	if err := n.check(); err != nil {
		return fmt.Errorf("to: %v", err)
	}
	if *n < another {
		return fmt.Errorf("invalid transition: backtracking %d state(s)", another-*n)
	}
	return nil
}

func (n *LogStateName) check() error {
	if *n < LogStatePending || *n > LogStateRejected {
		return fmt.Errorf("unsupported state name %q", *n)
	}
	return nil
}

// LogState implements the "state" object.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Must have exactly one named state which is known
//   - EnteredAt is non-zero on "date-time" JSON Schema format (Draft-07)
//   - The read-only state also has a tree head which contains a size >= zero
//     as well as a root hash which is exactly 44 characters long
//   - The root hash is in base64 (undocumented but evident from real metadata)
//   - A size of zero requires that the root hash is SHA256(""), and all other
//     sizes require a different root hash (RFC 6962, §2.1)
type LogState struct {
	Name      LogStateName
	EnteredAt time.Time

	// Available in the read-only state
	ReadOnlySize     uint64
	ReadOnlyRootHash [sha256.Size]byte
}

func (s *LogState) check() error {
	if err := s.Name.check(); err != nil {
		return fmt.Errorf("state: %v", err)
	}
	if s.EnteredAt.IsZero() {
		return fmt.Errorf("state: timestamp: is zero")
	}
	switch s.Name {
	case LogStateReadOnly:
		emptyTree := sha256.Sum256([]byte(""))
		if s.ReadOnlySize == 0 && emptyTree != s.ReadOnlyRootHash {
			return fmt.Errorf("state: final_tree_head: zero size with non-empty tree hash")
		}
		if s.ReadOnlySize != 0 && emptyTree == s.ReadOnlyRootHash {
			return fmt.Errorf("state: final_tree_head: non-zero size with empty tree hash")
		}
	default:
		zeroTree := [sha256.Size]byte{}
		if s.ReadOnlySize != 0 {
			return fmt.Errorf("state: only the readonly state has a tree size")
		}
		if s.ReadOnlyRootHash != zeroTree {
			return fmt.Errorf("state: only the readonly state has a root hash ")
		}
	}
	return nil
}

// PreviousOperator is an item in the "previous_operator" array.
//
// The following value constraints are enforced while (un)marshalling:
//
//   - Name is required on the wire (but may be "")
//   - End is non-zero on "date-time" JSON Schema format (Draft-07)
type PreviousOperator struct {
	Name string
	End  time.Time
}

func (o *PreviousOperator) check() error {
	if o.End.IsZero() {
		return fmt.Errorf("previous_operator: end_time: is zero")
	}
	return nil
}
