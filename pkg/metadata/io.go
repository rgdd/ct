package metadata

import (
	"context"
	"crypto"
	"crypto/ed25519"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"gitlab.torproject.org/rgdd/ct/internal/ctio"
)

// Loader is an interface that wraps the Load method.  Implementations of this
// interface must configure a public key as well as a location of the signed
// metadata object.  Examples of locations include web URLs and system files.
type Loader interface {
	// Load loads a signed metadata object from a source.  The raw metadata
	// and signature are returned so that they may be persisted.  A non-nil
	// error requires successful signature verification and unmarshalling.
	Load(ctx context.Context) ([]byte, []byte, Metadata, error)
}

// ErrLoad is an error while loading a signed metadata object.  If reading and
// signature verification succeeded, the cause of the error is unmarshalling.
type ErrLoad struct {
	ReadMetadata      bool
	ReadSignature     bool
	VerifiedSignature bool

	Message   []byte // Available if ReadMetadata is true
	Signature []byte // Available if ReadSignature is true
	GotError  error  // The error that caused Load() to stop
}

// Error formats an error message as a string
func (e ErrLoad) Error() string {
	if !e.ReadMetadata {
		return fmt.Sprintf("read: metadata: %v", e.GotError)
	}
	if !e.ReadSignature {
		return fmt.Sprintf("read: signature: %v", e.GotError)
	}
	if !e.VerifiedSignature {
		return fmt.Sprintf("verify: %v", e.GotError)
	}
	return fmt.Sprintf("unmarshal: %v", e.GotError)
}

// HTTPSource implements the Load interface for signed metadata objects that
// are locatable via URLs.  The following signature schemes are supported:
//
//   - Ed25519 (RFC 8032, §5.1)
//   - RSASSA-PKCS1-V1_5 with SHA256 (RFC 3447, §8.2)
//
// Use NewHTTPSource() to configure a new HTTP source with HTTPSourceOptions.
type HTTPSource struct {
	opts HTTPSourceOptions
}

// HTTPSourceOptions are options to configure an HTTP source
type HTTPSourceOptions struct {
	PublicKey    crypto.PublicKey
	MetadataURL  string
	SignatureURL string

	// Name is an optional name of a hardcoded parameter set that overrides
	// any specified public key, metadata URL, and signature URL.
	//
	// The supported parameter sets are: "google".
	Name string

	// UserAgent is an optional HTTP user agent (Default: ctio.HTTPUserAgent)
	UserAgent string

	// Timeout is an optional HTTP request timeout (Default: ctio.HTTPTimeout)
	Timeout time.Duration

	// Client is an optional HTTP client (Default: http.Client{})
	Client http.Client
}

// NewHTTPSource configures a new HTTP source based on the provided options
func NewHTTPSource(opts HTTPSourceOptions) HTTPSource {
	if opts.Name == "google" {
		opts.PublicKey, opts.MetadataURL, opts.SignatureURL = ctio.MetadataParamsGoogle()
	}
	if opts.UserAgent == "" {
		opts.UserAgent = ctio.HTTPUserAgent
	}
	if opts.Timeout == time.Duration(0) {
		opts.Timeout = ctio.HTTPTimeout
	}
	return HTTPSource{opts: opts}
}

// Load implements the Loader interface
func (s *HTTPSource) Load(ctx context.Context) ([]byte, []byte, Metadata, error) {
	msg, err := s.get(ctx, s.opts.MetadataURL)
	if err != nil {
		return nil, nil, Metadata{}, ErrLoad{false, false, false, nil, nil, err}
	}
	sig, err := s.get(ctx, s.opts.SignatureURL)
	if err != nil {
		return nil, nil, Metadata{}, ErrLoad{true, false, false, msg, nil, err}
	}
	if err := verify(s.opts.PublicKey, msg, sig); err != nil {
		return nil, nil, Metadata{}, ErrLoad{true, true, false, msg, sig, err}
	}
	var m Metadata
	if err := json.Unmarshal(msg, &m); err != nil {
		return nil, nil, Metadata{}, ErrLoad{true, true, true, msg, sig, err}
	}
	return msg, sig, m, nil
}

func (s *HTTPSource) get(ctx context.Context, url string) ([]byte, error) {
	ctxInner, cancel := context.WithTimeout(ctx, s.opts.Timeout)
	defer cancel()

	req, err := http.NewRequestWithContext(ctxInner, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	rsp, err := s.opts.Client.Do(req)
	if err != nil {
		return nil, err
	}
	defer rsp.Body.Close()
	if rsp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("status code %d", rsp.StatusCode)
	}
	return io.ReadAll(rsp.Body)
}

func verify(pk crypto.PublicKey, msg, sig []byte) error {
	switch t := pk.(type) {
	case *rsa.PublicKey:
		h := sha256.Sum256(msg)
		return rsa.VerifyPKCS1v15(pk.(*rsa.PublicKey), crypto.SHA256, h[:], sig)
	case ed25519.PublicKey:
		if !ed25519.Verify(pk.(ed25519.PublicKey), msg, sig) {
			return fmt.Errorf("invalid Ed25519 signature")
		}
		return nil
	default:
		return fmt.Errorf("unsupported public key type %T", t)
	}
}
