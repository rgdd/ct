package metadata

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"testing"
	"time"

	"gitlab.torproject.org/rgdd/ct/internal/tf"
)

func TestMetadataCheck(t *testing.T) {
	_, timeErr := testMetadata(t)
	timeErr.CreatedAt = time.Time{}

	_, dupName := testMetadata(t)
	_, tmp := testMetadata(t)
	tmp.Operators[0].Name = dupName.Operators[0].Name
	dupName.Operators = append(dupName.Operators, tmp.Operators[0])

	_, dupKey := testMetadata(t)
	tmp.Operators[0].Logs[0].Key = dupKey.Operators[0].Logs[0].Key
	dupKey.Operators[0].Logs = append(dupKey.Operators[0].Logs, tmp.Operators[0].Logs[0])

	_, metadata := testMetadata(t)
	for _, table := range []struct {
		desc  string
		input Metadata
	}{
		{"invalid: zero time", timeErr},
		{"invalid: dup operator", dupName},
		{"invalid: dup log", dupKey},
		{"valid", metadata},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestOperatorCheck(t *testing.T) {
	_, emailLenErr := testOperator(t)
	emailLenErr.Emails = []Email{}
	_, emailDupErr := testOperator(t)
	emailDupErr.Emails = append(emailDupErr.Emails, emailDupErr.Emails[0])
	_, logDupErr := testOperator(t)
	logDupErr.Logs = append(logDupErr.Logs, logDupErr.Logs[0])

	_, operator := testOperator(t)
	for _, table := range []struct {
		desc  string
		input Operator
	}{
		{"invalid: email len", emailLenErr},
		{"invalid: email dup", emailDupErr},
		{"invalid: log dup", logDupErr},
		{"valid", operator},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestEmailCheck(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input Email
	}{
		{"invalid: no '@'", "example.org"},
		{"valid: example", "foo@example.org"},
		{"valid: sad", "@"},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestLogCheck(t *testing.T) {
	_, keyErr := testLog(t)
	keyErr.Key.Public = nil
	_, histErr := testLog(t)
	*histErr.History = append(*histErr.History, (*histErr.History)[0])

	_, log := testLog(t)
	for _, table := range []struct {
		desc  string
		input Log
	}{
		{"invalid: key", keyErr},
		{"invalid: history", histErr},
		{"valid", log},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestLogUnique(t *testing.T) {
	_, log := testLog(t)
	id, _ := log.Key.ID()

	_, keyErr := testLog(t)
	keyErr.Key.Public = nil
	for _, table := range []struct {
		desc        string
		inputLog    Log
		inputIDMap  map[[sha256.Size]byte]bool
		inputURLMap map[LogURL]bool
		wantIDMap   map[[sha256.Size]byte]bool
		wantURLMap  map[LogURL]bool
	}{
		{
			desc:     "invalid: key",
			inputLog: keyErr,
		},
		{
			desc:        "invalid: duplicate log id",
			inputLog:    log,
			inputIDMap:  map[[sha256.Size]byte]bool{id: true},
			inputURLMap: make(map[LogURL]bool),
		},
		{
			desc:        "invalid: duplicate url",
			inputLog:    log,
			inputIDMap:  make(map[[sha256.Size]byte]bool),
			inputURLMap: map[LogURL]bool{log.URL: true},
		},
		{
			desc:        "valid",
			inputLog:    log,
			inputIDMap:  make(map[[sha256.Size]byte]bool),
			inputURLMap: make(map[LogURL]bool),
			wantIDMap:   map[[sha256.Size]byte]bool{id: true},
			wantURLMap:  map[LogURL]bool{log.URL: true},
		},
	} {
		if tf.Error(t, table.desc, table.inputLog.unique(table.inputIDMap, table.inputURLMap)) {
			continue
		}
		tf.Equal(t, table.desc, table.inputIDMap, table.wantIDMap)
		tf.Equal(t, table.desc, table.inputURLMap, table.wantURLMap)
	}
}

func TestLogKeyID(t *testing.T) {
	id, _, pub, _ := tf.ECDSA(t)
	for _, table := range []struct {
		desc  string
		input LogKey
		want  [sha256.Size]byte
	}{
		{"invalid: marshal", LogKey{}, [sha256.Size]byte{}},
		{"invalid: check", LogKey{ecdsaCurve224(t)}, [sha256.Size]byte{}},
		{"valid", LogKey{pub}, id},
	} {
		got, err := table.input.ID()
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestLogKeyCheck(t *testing.T) {
	_, _, ed25519Pub, _ := tf.Ed25519(t)
	_, _, ecdsaPub, _ := tf.ECDSA(t)
	_, _, rsaPub, _ := tf.RSA(t, 2048)
	_, _, rsaFail, _ := tf.RSA(t, 2047)
	for _, table := range []struct {
		desc  string
		input LogKey
	}{
		{"invalid: nil", LogKey{}},
		{"invalid: ecdsa curve", LogKey{ecdsaCurve224(t)}},
		{"invalid: rsa bits", LogKey{rsaFail}},
		{"invalid: key type", LogKey{ed25519Pub}},
		{"valid: ecdsa", LogKey{ecdsaPub}},
		{"valid: rsa", LogKey{rsaPub}},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestLogMMDCheck(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input LogMMD
	}{
		{"invalid: zero", LogMMD(0)},
		{"valid: default", defaultLogMMD},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestLogULRCheck(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input LogURL
	}{
		{"invalid: start", "http://example.org"},
		{"invalid: middle", "https:///"},
		{"invalid: end", "https://example.org"},
		{"valid: short", "https://example.org/"},
		{"valid: long", "https://ct.example.org:8000/logs/2020/"},
		{"valid: sad", "https:// /"},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestLogDNSNameCheck(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input LogDNSName
	}{
		{"invalid", ""},
		{"valid: normal", "ct.example.org"},
		{"valid: sad", " "},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestLogShardCheck(t *testing.T) {
	start := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	end := time.Date(2020, 12, 31, 0, 0, 0, 0, time.UTC)
	for _, table := range []struct {
		desc  string
		input LogShard
	}{
		{"invalid: zero start", LogShard{time.Time{}, end}},
		{"invalid: zero end", LogShard{start, time.Time{}}},
		{"invalid: start>end", LogShard{end, end}},
		{"valid", LogShard{start, end}},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestLogTypeCheck(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input LogType
	}{
		{"invalid: unknown type", LogType("")},
		{"valid: test", LogTypeTest},
		{"valid: prod", LogTypeProd},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestLogStateNameValidFrom(t *testing.T) {
	for _, table := range []struct {
		desc string
		from LogStateName
		to   LogStateName
	}{
		{"invalid: unknown from state", LogStateName(0), LogStatePending},
		{"invalid: unknown to state", LogStatePending, LogStateName(0)},
		// pending
		{"valid: pending -> pending", LogStatePending, LogStatePending},
		{"valid: pending -> qualified", LogStatePending, LogStateQualified},
		{"valid: pending -> usable", LogStatePending, LogStateUsable},
		{"valid: pending -> read-only", LogStatePending, LogStateReadOnly},
		{"valid: pending -> retired", LogStatePending, LogStateRetired},
		{"valid: pending -> rejected", LogStatePending, LogStateRejected},
		// qualified
		{"invalid: qualified -> pending", LogStateQualified, LogStatePending},
		{"valid: qualified -> qualified", LogStateQualified, LogStateQualified},
		{"valid: qualified -> usable", LogStateQualified, LogStateUsable},
		{"valid: qualified -> read-only", LogStateQualified, LogStateReadOnly},
		{"valid: qualified -> retired", LogStateQualified, LogStateRetired},
		{"valid: qualified -> rejected", LogStateQualified, LogStateRejected},
		// usable
		{"invalid: usable -> pending", LogStateUsable, LogStatePending},
		{"invalid: usable -> qualified", LogStateUsable, LogStateQualified},
		{"valid: usable -> usable", LogStateUsable, LogStateUsable},
		{"valid: usable -> read-only", LogStateUsable, LogStateReadOnly},
		{"valid: usable -> retired", LogStateUsable, LogStateRetired},
		{"valid: usable -> rejected", LogStateUsable, LogStateRejected},
		// read-only
		{"invalid: read-only -> pending", LogStateReadOnly, LogStatePending},
		{"invalid: read-only -> qualified", LogStateReadOnly, LogStateQualified},
		{"invalid: read-only -> usable", LogStateReadOnly, LogStateUsable},
		{"valid: read-only -> read-only", LogStateReadOnly, LogStateReadOnly},
		{"valid: read-only -> retired", LogStateReadOnly, LogStateRetired},
		{"valid: read-only -> rejected", LogStateReadOnly, LogStateRejected},
		// retired
		{"invalid: retired -> pending", LogStateRetired, LogStatePending},
		{"invalid: retired -> qualified", LogStateRetired, LogStateQualified},
		{"invalid: retired -> usable", LogStateRetired, LogStateUsable},
		{"invalid: retired -> read-only", LogStateRetired, LogStateReadOnly},
		{"valid: retired -> retired", LogStateRetired, LogStateRetired},
		{"valid: retired -> rejected", LogStateRetired, LogStateRejected},
		// rejected
		{"invalid: rejected -> pending", LogStateRejected, LogStatePending},
		{"invalid: rejected -> qualified", LogStateRejected, LogStateQualified},
		{"invalid: rejected -> usable", LogStateRejected, LogStateUsable},
		{"invalid: rejected -> read-only", LogStateRejected, LogStateReadOnly},
		{"invalid: rejected-> retired", LogStateRejected, LogStateRetired},
		{"valid: rejected -> rejected", LogStateRejected, LogStateRejected},
	} {
		tf.Error(t, table.desc, table.to.ValidFrom(table.from))
	}
}

func TestLogStateNameCheck(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input LogStateName
	}{
		{"invalid: unknown state (low)", LogStatePending - 1},
		{"invalid: unknown state (high)", LogStateRejected + 1},
		{"valid: pending", LogStatePending},
		{"valid: qualified", LogStateQualified},
		{"valid: usable", LogStateUsable},
		{"valid: read-only", LogStateReadOnly},
		{"valid: retired", LogStateRetired},
		{"valid: rejected", LogStateRejected},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestLogStateCheck(t *testing.T) {
	d := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	e := [sha256.Size]byte{}
	h := sha256.Sum256(nil)
	for _, table := range []struct {
		desc  string
		input LogState
	}{
		{"invalid: state name", LogState{Name: LogStateName(0), EnteredAt: d}},
		{"invalid: timestamp", LogState{Name: LogStatePending}},
		{"invalid: read-only: size+root (1/2)", LogState{Name: LogStateReadOnly, EnteredAt: d, ReadOnlySize: 0, ReadOnlyRootHash: e}},
		{"invalid: read-only: size+root (2/2)", LogState{Name: LogStateReadOnly, EnteredAt: d, ReadOnlySize: 1, ReadOnlyRootHash: h}},
		{"invalid: non-zero size", LogState{Name: LogStatePending, EnteredAt: d, ReadOnlySize: 1}},
		{"invalid: non-zero root", LogState{Name: LogStatePending, EnteredAt: d, ReadOnlyRootHash: h}},
		{"valid", LogState{Name: LogStatePending, EnteredAt: d}},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func TestLogPreviousOperatorCheck(t *testing.T) {
	d := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	for _, table := range []struct {
		desc  string
		input PreviousOperator
	}{
		{"invalid: no end", PreviousOperator{"", time.Time{}}},
		{"valid", PreviousOperator{"", d}},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}

func ecdsaCurve224(t *testing.T) crypto.PublicKey {
	k, err := ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	if err != nil {
		t.Fatal(err)
	}
	return k.Public()
}

func testMetadata(t *testing.T) (metadata, Metadata) {
	_, op := testOperator(t)

	version := Version{1, 2}
	createdAt := time.Date(2020, 01, 01, 0, 0, 0, 0, time.UTC)
	operators := []Operator{op}
	mi := metadata{&version, &createdAt, &operators}
	mp := Metadata{version, createdAt, operators}
	return mi, mp
}

func testOperator(t *testing.T) (operator, Operator) {
	_, lp := testLog(t)

	name := "Foo"
	emails := []Email{"a@foo.org"}
	logs := []Log{lp}
	oi := operator{&name, &emails, &logs}
	op := Operator{name, emails, logs}
	return oi, op
}

// testLog generates a new test log such that public keys and URLs are unique
func testLog(t *testing.T) (log, Log) {
	i, _, k, _ := tf.ECDSA(t)
	start := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	end := time.Date(2020, 12, 31, 0, 0, 0, 0, time.UTC)

	key := LogKey{Public: k}
	id := i[:]
	mmd := defaultLogMMD + 1
	url := LogURL(fmt.Sprintf("https://%x.%x.example.org/", id[:32], id[32:]))
	desc := "foo"
	shard := LogShard{Start: start, End: end}
	typ3 := LogTypeTest
	state := LogState{Name: LogStatePending, EnteredAt: start}
	history := []PreviousOperator{PreviousOperator{Name: "bar", End: start}}
	dns := LogDNSName("example.org")
	li := log{&key, &id, &mmd, &url, &desc, &typ3, &shard, &state, &history, &dns}
	lp := Log{key, mmd, url, &desc, &typ3, &shard, &state, &history, &dns}
	return li, lp
}
