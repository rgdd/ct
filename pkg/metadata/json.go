package metadata

//
// This file defines strict JSON (un)marshalling for all public types.
//

import (
	"encoding/json"
	"fmt"
)

// UnmarshalJSON unmarshals the top-most metadata object
func (m *Metadata) UnmarshalJSON(b []byte) (err error) {
	var internal metadata
	if err := json.Unmarshal(b, &internal); err != nil {
		return fmt.Errorf("metadata: %v", err)
	}
	return m.parse(internal)
}

// UnmarshalJSON unmarshals the "version" string
func (v *Version) UnmarshalJSON(b []byte) error {
	var internal string
	if err := json.Unmarshal(b, &internal); err != nil {
		return fmt.Errorf("version: %v", err)
	}
	return v.parse(internal)
}

// UnmarshalJSON unmarshals an item in the "operators" array
func (o *Operator) UnmarshalJSON(b []byte) (err error) {
	var internal operator
	if err := json.Unmarshal(b, &internal); err != nil {
		return fmt.Errorf("operator: %v", err)
	}
	return o.parse(internal)
}

// UnmarshalJSON unmarshals an item in the "email" array
func (e *Email) UnmarshalJSON(b []byte) error {
	if err := json.Unmarshal(b, (*string)(e)); err != nil {
		return fmt.Errorf("email: %v", err)
	}
	return e.check()
}

// UnmarshalJSON unmarshals an item in the "logs" array
func (l *Log) UnmarshalJSON(b []byte) (err error) {
	var internal log
	if err := json.Unmarshal(b, &internal); err != nil {
		return fmt.Errorf("log: %v", err)
	}
	return l.parse(internal)
}

// UnmarshalJSON unmarshals the "key" string
func (k *LogKey) UnmarshalJSON(b []byte) (err error) {
	var internal []byte
	if err := json.Unmarshal(b, &internal); err != nil {
		return fmt.Errorf("key: %v", err)
	}
	return k.parse(internal)
}

// UnmarshalJSON unmarshals the "mmd" number
func (m *LogMMD) UnmarshalJSON(b []byte) error {
	if err := json.Unmarshal(b, (*uint64)(m)); err != nil {
		return fmt.Errorf("mmd: %v", err)
	}
	return m.check()
}

// UnmarshalJSON unmarshals the "url" string
func (u *LogURL) UnmarshalJSON(b []byte) error {
	if err := json.Unmarshal(b, (*string)(u)); err != nil {
		return fmt.Errorf("url: %v", err)
	}
	return u.check()
}

// UnmarshalJSON unmarshals the "dns" string
func (n *LogDNSName) UnmarshalJSON(b []byte) error {
	if err := json.Unmarshal(b, (*string)(n)); err != nil {
		return fmt.Errorf("dns: %v", err)
	}
	return n.check()
}

// UnmarshalJSON umarshals the "temporal_interval" object
func (s *LogShard) UnmarshalJSON(b []byte) (err error) {
	var internal logShard
	if err := json.Unmarshal(b, &internal); err != nil {
		return fmt.Errorf("temporal_interval: %v", err)
	}
	return s.parse(internal)
}

// UnmarshalJSON unmarshals the "log_type" string
func (t *LogType) UnmarshalJSON(b []byte) error {
	if err := json.Unmarshal(b, (*string)(t)); err != nil {
		return fmt.Errorf("log_type: %v", err)
	}
	return t.check()
}

// UnmarshalJSON unmarshals the "state" object
func (s *LogState) UnmarshalJSON(b []byte) (err error) {
	var internal logState
	if err = json.Unmarshal(b, &internal); err != nil {
		return fmt.Errorf("state: %v", err)
	}
	return s.parse(internal)
}

// UnmarshalJSON unmarshals an item in the "previous_operators" array
func (o *PreviousOperator) UnmarshalJSON(b []byte) (err error) {
	var internal previousOperator
	if err := json.Unmarshal(b, &internal); err != nil {
		return fmt.Errorf("previous_operator: %v", err)
	}
	return o.parse(internal)
}

// MarshalJSON marshals the top-most metadata object
func (m *Metadata) MarshalJSON() ([]byte, error) {
	internal, err := m.internal()
	if err != nil {
		return nil, err
	}
	return json.Marshal(internal)
}

// MarshalJSON marshals the "version" string
func (v *Version) MarshalJSON() ([]byte, error) {
	return json.Marshal(v.internal())
}

// MarshalJSON marshals an item in the "operators" array
func (o *Operator) MarshalJSON() ([]byte, error) {
	internal, err := o.internal()
	if err != nil {
		return nil, err
	}
	return json.Marshal(internal)
}

// MarshalJSON marshals the "email" string
func (e *Email) MarshalJSON() ([]byte, error) {
	if err := e.check(); err != nil {
		return nil, err
	}
	return json.Marshal(*e)
}

// MarshalJSON marshals an item in the "logs" array
func (l *Log) MarshalJSON() ([]byte, error) {
	internal, err := l.internal()
	if err != nil {
		return nil, err
	}
	return json.Marshal(internal)
}

// MarshalJSON marshals the "key" string
func (k *LogKey) MarshalJSON() ([]byte, error) {
	internal, err := k.internal()
	if err != nil {
		return nil, err
	}
	return json.Marshal(internal)
}

// MarshalJSON marshals the "mmd" number
func (m *LogMMD) MarshalJSON() ([]byte, error) {
	if err := m.check(); err != nil {
		return nil, err
	}
	return json.Marshal(*m)
}

// MarshalJSON marshals the "url" string
func (u *LogURL) MarshalJSON() ([]byte, error) {
	if err := u.check(); err != nil {
		return nil, err
	}
	return json.Marshal(*u)
}

// MarshalJSON marshals the "dns" string
func (n *LogDNSName) MarshalJSON() ([]byte, error) {
	if err := n.check(); err != nil {
		return nil, err
	}
	return json.Marshal(*n)
}

// MarshalJSON marshals the "temporal_interval" object
func (s *LogShard) MarshalJSON() ([]byte, error) {
	internal, err := s.internal()
	if err != nil {
		return nil, err
	}
	return json.Marshal(internal)
}

// MarshalJSON marshals the "log_type" string
func (t *LogType) MarshalJSON() ([]byte, error) {
	if err := t.check(); err != nil {
		return nil, err
	}
	return json.Marshal(*t)
}

// MarshalJSON marshals the "state" object
func (s *LogState) MarshalJSON() ([]byte, error) {
	internal, err := s.internal()
	if err != nil {
		return nil, err
	}
	return json.Marshal(internal)
}

// MarshalJSON marshals an item in the "previous_operators" array
func (o *PreviousOperator) MarshalJSON() ([]byte, error) {
	internal, err := o.internal()
	if err != nil {
		return nil, err
	}
	return json.Marshal(internal)
}
