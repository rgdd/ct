package metadata

//
// This file defines internal versions of the objects in metadata.go.  All
// fields are pointers with JSON tags so that (un)set fields can be detected.
//
// There's also some behind-the-scene plumbing taking place so that the public
// types have fields that are easy to use (see for example the logState object).
//

import (
	"bytes"
	"crypto/sha256"
	"crypto/x509"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type metadata struct {
	Version   *Version    `json:"version"`
	Timestamp *time.Time  `json:"log_list_timestamp"`
	Operators *[]Operator `json:"operators"`
}

func (m *Metadata) parse(i metadata) error {
	if i.Version == nil {
		return fmt.Errorf("metadata: version: is required")
	}
	if i.Timestamp == nil {
		return fmt.Errorf("metadata: log_list_timestamp: is required")
	}
	if i.Operators == nil {
		return fmt.Errorf("metadata: operators: is required")
	}

	*m = Metadata{*i.Version, *i.Timestamp, *i.Operators}
	return m.check()
}

func (m *Metadata) internal() (metadata, error) {
	return metadata{&m.Version, &m.CreatedAt, &m.Operators}, m.check()
}

func (v *Version) parse(i string) (err error) {
	split := strings.Split(i, ".")
	if len(split) != 2 {
		return fmt.Errorf("version: format is not <major>.<minor>")
	}
	// TODO: https://datatracker.ietf.org/doc/html/rfc7159#section-6
	if v.Major, err = strconv.ParseUint(split[0], 10, 64); err != nil {
		return fmt.Errorf("version: major: %v", err)
	}
	if v.Minor, err = strconv.ParseUint(split[1], 10, 64); err != nil {
		return fmt.Errorf("version: minor: %v", err)
	}
	return nil
}

func (v *Version) internal() string {
	return fmt.Sprintf("%d.%d", v.Major, v.Minor)
}

type operator struct {
	Name   *string  `json:"name"`
	Emails *[]Email `json:"email"`
	Logs   *[]Log   `json:"logs"`
}

func (o *Operator) parse(i operator) error {
	if i.Name == nil {
		return fmt.Errorf("operator: name: is required")
	}
	if i.Emails == nil {
		return fmt.Errorf("operator: email: is required")
	}
	if i.Logs == nil {
		return fmt.Errorf("operator: logs: is required")
	}

	*o = Operator{*i.Name, *i.Emails, *i.Logs}
	return o.check()
}

func (o *Operator) internal() (operator, error) {
	return operator{&o.Name, &o.Emails, &o.Logs}, o.check()
}

type log struct {
	Key *LogKey `json:"key"`
	ID  *[]byte `json:"log_id"`
	MMD *LogMMD `json:"mmd"`
	URL *LogURL `json:"url"`

	Description *string             `json:"description,omitempty"`
	Type        *LogType            `json:"log_type,omitempty"`
	Shard       *LogShard           `json:"temporal_interval,omitempty"`
	State       *LogState           `json:"state,omitempty"`
	History     *[]PreviousOperator `json:"previous_operators"`
	DNS         *LogDNSName         `json:"dns,omitempty"`
}

func (l *Log) parse(i log) error {
	if i.Key == nil {
		return fmt.Errorf("log: key: is required")
	}
	if i.ID == nil {
		return fmt.Errorf("log: log_id: is required")
	}
	if i.MMD == nil {
		mmd := defaultLogMMD
		i.MMD = &mmd
	}
	if i.URL == nil {
		return fmt.Errorf("log: url: is required")
	}

	//
	// Check that log ID matches key ID.  This has to be done here because
	// we don't have a (redundant) log ID field in the public Log type.
	//
	id, err := i.Key.ID()
	if err != nil {
		panic(fmt.Sprintf("BUG: log: key: %v", err))
	}
	if !bytes.Equal(id[:], *i.ID) {
		return fmt.Errorf("log: log_id: not equal to key id")
	}

	*l = Log{*i.Key, *i.MMD, *i.URL, i.Description, i.Type, i.Shard, i.State, i.History, i.DNS}
	return l.check()
}

func (l *Log) internal() (log, error) {
	keyID, err := l.Key.ID()
	if err != nil {
		return log{}, fmt.Errorf("log: key: %v", err)
	}

	logID := keyID[:]
	return log{&l.Key, &logID, &l.MMD, &l.URL, l.Description, l.Type, l.Shard, l.State, l.History, l.DNS}, l.check()
}

func (k *LogKey) parse(i []byte) (err error) {
	if k.Public, err = x509.ParsePKIXPublicKey(i); err != nil {
		return fmt.Errorf("key: %v", err)
	}
	return k.check()
}

func (k *LogKey) internal() ([]byte, error) {
	der, err := x509.MarshalPKIXPublicKey(k.Public)
	if err != nil {
		return nil, fmt.Errorf("key: %v", err)
	}
	return der, k.check()
}

type logShard struct {
	Start *time.Time `json:"start_inclusive"`
	End   *time.Time `json:"end_exclusive"`
}

func (s *LogShard) parse(i logShard) error {
	if i.Start == nil {
		return fmt.Errorf("temporal_interval: start_inclusive: is required")
	}
	if i.End == nil {
		return fmt.Errorf("temporal_interval: end_exclusive: is required")
	}

	*s = LogShard{*i.Start, *i.End}
	return s.check()
}

func (s *LogShard) internal() (logShard, error) {
	return logShard{&s.Start, &s.End}, s.check()
}

type logState struct {
	Pending   *logStateTimestamp `json:"pending,omitempty"`
	Qualified *logStateTimestamp `json:"qualified,omitempty"`
	Usable    *logStateTimestamp `json:"usable,omitempty"`
	ReadOnly  *logStateReadOnly  `json:"readonly,omitempty"`
	Retired   *logStateTimestamp `json:"retired,omitempty"`
	Rejected  *logStateTimestamp `json:"rejected,omitempty"`
}

type logStateTimestamp struct {
	Timestamp *time.Time `json:"timestamp"`
}

type logStateReadOnly struct {
	Timestamp *time.Time                `json:"timestamp"`
	TreeHead  *logStateReadOnlyTreeHead `json:"final_tree_head"`
}

func (s *LogState) parse(i logState) (err error) {
	var setOnce bool
	set := func(name LogStateName, enteredAt *time.Time) error {
		if setOnce {
			return fmt.Errorf("state: more than one")
		}

		setOnce = true
		if enteredAt == nil {
			return fmt.Errorf("timestamp: is required")
		}

		if name == LogStateReadOnly {
			if i.ReadOnly.TreeHead == nil {
				return fmt.Errorf("state: readonly: final_tree_head: is required")
			}
			if s.ReadOnlySize, s.ReadOnlyRootHash, err = i.ReadOnly.TreeHead.parse(); err != nil {
				return fmt.Errorf("state: readonly: final_tree_head: %v", err)
			}
		}

		s.EnteredAt = *enteredAt
		s.Name = name
		return nil
	}

	if i.Pending != nil {
		err = set(LogStatePending, i.Pending.Timestamp)
	}
	if i.Usable != nil {
		err = set(LogStateUsable, i.Usable.Timestamp)
	}
	if i.Qualified != nil {
		err = set(LogStateQualified, i.Qualified.Timestamp)
	}
	if i.ReadOnly != nil {
		err = set(LogStateReadOnly, i.ReadOnly.Timestamp)
	}
	if i.Retired != nil {
		err = set(LogStateRetired, i.Retired.Timestamp)
	}
	if i.Rejected != nil {
		err = set(LogStateRejected, i.Rejected.Timestamp)
	}
	if err != nil {
		return err
	}

	return s.check()
}

func (s *LogState) internal() (logState, error) {
	switch s.Name {
	case LogStatePending:
		return logState{Pending: &logStateTimestamp{&s.EnteredAt}}, s.check()
	case LogStateQualified:
		return logState{Qualified: &logStateTimestamp{&s.EnteredAt}}, s.check()
	case LogStateUsable:
		return logState{Usable: &logStateTimestamp{&s.EnteredAt}}, s.check()
	case LogStateReadOnly:
		r := s.ReadOnlyRootHash[:]
		th := logStateReadOnlyTreeHead{&s.ReadOnlySize, &r}
		return logState{ReadOnly: &logStateReadOnly{&s.EnteredAt, &th}}, s.check()
	case LogStateRetired:
		return logState{Retired: &logStateTimestamp{&s.EnteredAt}}, s.check()
	case LogStateRejected:
		return logState{Rejected: &logStateTimestamp{&s.EnteredAt}}, s.check()
	default:
		return logState{}, fmt.Errorf("state: %d is not supported", s.Name)
	}
}

type logStateReadOnlyTreeHead struct {
	Size     *uint64 `json:"tree_size"`
	RootHash *[]byte `json:"sha256_root_hash"`
}

func (th *logStateReadOnlyTreeHead) parse() (size uint64, root [sha256.Size]byte, err error) {
	if th.Size == nil {
		return size, root, fmt.Errorf("tree_size: is required")
	}
	if th.RootHash == nil {
		return size, root, fmt.Errorf("sha256_root_hash: is required")
	}

	//
	// Check that we have the right number of bytes.  This has to be done
	// here because we don't have an (error-prone) byte-slice in type State.
	//
	if len(*th.RootHash) != 32 {
		return size, root, fmt.Errorf("sha256_root_hash: invalid size")
	}

	size = *th.Size
	copy(root[:], *th.RootHash)
	return
}

type previousOperator struct {
	Name *string    `json:"name"`
	End  *time.Time `json:"end_time"`
}

func (o *PreviousOperator) parse(i previousOperator) error {
	if i.Name == nil {
		return fmt.Errorf("name: is required")
	}
	if i.End == nil {
		return fmt.Errorf("end_time: is required")
	}

	*o = PreviousOperator{*i.Name, *i.End}
	return o.check()
}

func (o *PreviousOperator) internal() (previousOperator, error) {
	return previousOperator{&o.Name, &o.End}, o.check()
}
