package metadata

import (
	"crypto/sha256"
	"testing"
	"time"

	"gitlab.torproject.org/rgdd/ct/internal/tf"
)

func TestMetadataParse(t *testing.T) {
	mi, mp := testMetadata(t)
	zero := time.Time{}
	for _, table := range []struct {
		desc  string
		input metadata
		want  Metadata
	}{
		{"invalid: nil version", metadata{nil, mi.Timestamp, mi.Operators}, Metadata{}},
		{"invalid: nil timestamp", metadata{mi.Version, nil, mi.Operators}, Metadata{}},
		{"invalid: nil operators", metadata{mi.Version, mi.Timestamp, nil}, Metadata{}},
		{"invalid: check", metadata{mi.Version, &zero, mi.Operators}, Metadata{}},
		{"valid", mi, mp},
	} {
		var got Metadata
		if tf.Error(t, table.desc, got.parse(table.input)) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestMetadataInternal(t *testing.T) {
	mi, mp := testMetadata(t)
	for _, table := range []struct {
		desc  string
		input Metadata
		want  metadata
	}{
		{"invalid: check", Metadata{Version: mp.Version, CreatedAt: time.Time{}, Operators: mp.Operators}, metadata{}},
		{"valid", mp, mi},
	} {
		got, err := table.input.internal()
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestVersionParse(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  Version
	}{
		{"invalid: split none", "1234", Version{}},
		{"invalid: split many", "1.2.34", Version{}},
		{"invalid: major \"\"", ".34", Version{}},
		{"invalid: major neg", "-1.34", Version{}},
		{"invalid: major sign", "+1.34", Version{}},
		{"invalid: minor \"\"", "12.", Version{}},
		{"invalid: minor neg", "12.-1", Version{}},
		{"invalid: minor sign", "12.+1", Version{}},
		{"valid: normal", "12.34", Version{12, 34}},
		{"valid: sad", "012.034", Version{12, 34}},
	} {
		var got Version
		if tf.Error(t, table.desc, got.parse(table.input)) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)

	}
}

func TestVersionInternal(t *testing.T) {
	tf.Equal(t, "valid", (&Version{12, 34}).internal(), "12.34")
}

func TestOperatorParse(t *testing.T) {
	oi, op := testOperator(t)
	failEmail := []Email{}
	for _, table := range []struct {
		desc  string
		input operator
		want  Operator
	}{
		{"invalid: nil name", operator{nil, oi.Emails, oi.Logs}, Operator{}},
		{"invalid: nil email", operator{oi.Name, nil, oi.Logs}, Operator{}},
		{"invalid: nil logs", operator{oi.Name, oi.Emails, nil}, Operator{}},
		{"invalid: check", operator{oi.Name, &failEmail, oi.Logs}, Operator{}},
		{"valid", oi, op},
	} {
		var got Operator
		if tf.Error(t, table.desc, got.parse(table.input)) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestOperatorInternal(t *testing.T) {
	oi, op := testOperator(t)
	for _, table := range []struct {
		desc  string
		input Operator
		want  operator
	}{
		{"invalid: check", Operator{op.Name, []Email{}, op.Logs}, operator{}},
		{"valid", op, oi},
	} {
		got, err := table.input.internal()
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestLogParse(t *testing.T) {
	li, lp := testLog(t)

	id := sha256.Sum256([]byte("foo"))
	failID := id[:]
	failHist := append(*li.History, (*li.History)[0])
	for _, table := range []struct {
		desc  string
		input log
		want  Log
	}{
		{"invalid: nil key", log{ID: li.ID, URL: li.URL, MMD: li.MMD}, Log{}},
		{"invalid: nil id", log{Key: li.Key, URL: li.URL, MMD: li.MMD}, Log{}},
		{"invalid: nil url", log{Key: li.Key, ID: li.ID, MMD: li.MMD}, Log{}},
		{"invalid: id", log{Key: li.Key, ID: &failID, URL: li.URL, MMD: li.MMD}, Log{}},
		{"invalid: check", log{Key: li.Key, ID: li.ID, URL: li.URL, MMD: li.MMD, History: &failHist}, Log{}},
		{"valid: default mmd", log{Key: li.Key, ID: li.ID, URL: li.URL}, Log{Key: lp.Key, MMD: defaultLogMMD, URL: lp.URL}},
		{"valid: all", li, lp},
	} {
		var got Log
		if tf.Error(t, table.desc, got.parse(table.input)) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestLogInternal(t *testing.T) {
	li, lp := testLog(t)
	for _, table := range []struct {
		desc  string
		input Log
		want  log
	}{
		{"invalid: key", Log{Key: LogKey{Public: nil}, MMD: lp.MMD, URL: lp.URL}, log{}},
		{"valid", lp, li},
	} {
		got, err := table.input.internal()
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestLogKeyParse(t *testing.T) {
	_, der, pub, _ := tf.ECDSA(t)
	_, failBytes, failKey, _ := tf.Ed25519(t)
	for _, table := range []struct {
		desc  string
		input []byte
		want  LogKey
	}{
		{"invalid: parse", []byte("foo"), LogKey{}},
		{"invalid: check", failBytes, LogKey{failKey}},
		{"valid", der, LogKey{pub}},
	} {
		var got LogKey
		if tf.Error(t, table.desc, got.parse(table.input)) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestLogKeyInternal(t *testing.T) {
	_, der, pub, _ := tf.ECDSA(t)
	_, _, fail, _ := tf.Ed25519(t)
	for _, table := range []struct {
		desc  string
		input LogKey
		want  []byte
	}{
		{"invalid: marshal", LogKey{nil}, nil},
		{"invalid: check", LogKey{fail}, nil},
		{"valid", LogKey{pub}, der},
	} {
		got, err := table.input.internal()
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestLogShardParse(t *testing.T) {
	start := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	end := time.Date(2020, 12, 31, 0, 0, 0, 0, time.UTC)
	for _, table := range []struct {
		desc  string
		input logShard
		want  LogShard
	}{
		{"invalid: nil start", logShard{nil, &end}, LogShard{}},
		{"invalid: nil end", logShard{&start, nil}, LogShard{}},
		{"invalid: check", logShard{&end, &start}, LogShard{}},
		{"valid", logShard{&start, &end}, LogShard{start, end}},
	} {
		var got LogShard
		if tf.Error(t, table.desc, got.parse(table.input)) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestLogShardInternal(t *testing.T) {
	start := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	end := time.Date(2020, 12, 31, 0, 0, 0, 0, time.UTC)
	for _, table := range []struct {
		desc  string
		input LogShard
		want  logShard
	}{
		{"invalid: check", LogShard{time.Time{}, end}, logShard{}},
		{"valid", LogShard{start, end}, logShard{&start, &end}},
	} {
		got, err := table.input.internal()
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestLogStateParse(t *testing.T) {
	d := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	s := uint64(1)
	h := sha256.Sum256([]byte("foo"))
	hs := h[:]
	th := logStateReadOnlyTreeHead{&s, &hs}

	hl := h[1:]
	thFail := logStateReadOnlyTreeHead{&s, &hl}
	for _, table := range []struct {
		desc  string
		input logState
		want  LogState
	}{
		{"invalid: num states", logState{Pending: &logStateTimestamp{&d}, Qualified: &logStateTimestamp{&d}}, LogState{}},
		{"invalid: unknown state", logState{}, LogState{}},
		// pending
		{"invalid: pending", logState{Pending: &logStateTimestamp{}}, LogState{}},
		{"valid: pending", logState{Pending: &logStateTimestamp{&d}}, LogState{Name: LogStatePending, EnteredAt: d}},
		// qualified
		{"invalid: qualified", logState{Qualified: &logStateTimestamp{}}, LogState{}},
		{"valid: qualified", logState{Qualified: &logStateTimestamp{&d}}, LogState{Name: LogStateQualified, EnteredAt: d}},
		// usable
		{"invalid: usable", logState{Usable: &logStateTimestamp{}}, LogState{}},
		{"valid: usable", logState{Usable: &logStateTimestamp{&d}}, LogState{Name: LogStateUsable, EnteredAt: d}},
		// read-only
		{"invalid: readonly: timestamp", logState{ReadOnly: &logStateReadOnly{nil, &th}}, LogState{}},
		{"invalid: readonly: nil head", logState{ReadOnly: &logStateReadOnly{&d, nil}}, LogState{}},
		{"invalid: readonly: parse head", logState{ReadOnly: &logStateReadOnly{&d, &thFail}}, LogState{}},
		{"valid: readonly", logState{ReadOnly: &logStateReadOnly{&d, &th}}, LogState{LogStateReadOnly, d, s, h}},
		// retired
		{"invalid: retired", logState{Retired: &logStateTimestamp{}}, LogState{}},
		{"valid: retired", logState{Retired: &logStateTimestamp{&d}}, LogState{Name: LogStateRetired, EnteredAt: d}},
		// rejected
		{"invalid: rejected", logState{Rejected: &logStateTimestamp{}}, LogState{}},
		{"valid: rejected", logState{Rejected: &logStateTimestamp{&d}}, LogState{Name: LogStateRejected, EnteredAt: d}},
	} {
		var got LogState
		if tf.Error(t, table.desc, got.parse(table.input)) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestLogStateInternal(t *testing.T) {
	d := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	s := uint64(0)
	h := sha256.Sum256(nil)
	hs := h[:]
	th := logStateReadOnlyTreeHead{&s, &hs}
	for _, table := range []struct {
		desc  string
		input LogState
		want  logState
	}{
		{"invalid: state name", LogState{Name: LogStateName(0), EnteredAt: d}, logState{}},
		// pending
		{"invalid: pending timestamp", LogState{Name: LogStatePending}, logState{}},
		{"valid: pending", LogState{Name: LogStatePending, EnteredAt: d}, logState{Pending: &logStateTimestamp{&d}}},
		// qualified
		{"invalid: qualified timestamp", LogState{Name: LogStateQualified}, logState{}},
		{"valid: qualified", LogState{Name: LogStateQualified, EnteredAt: d}, logState{Qualified: &logStateTimestamp{&d}}},
		// usable
		{"invalid: usable timestamp", LogState{Name: LogStateUsable}, logState{}},
		{"valid: usable", LogState{Name: LogStateUsable, EnteredAt: d}, logState{Usable: &logStateTimestamp{&d}}},
		// read-only
		{"invalid: read-only timestamp", LogState{Name: LogStateReadOnly}, logState{}},
		{"valid: read-only", LogState{LogStateReadOnly, d, s, h}, logState{ReadOnly: &logStateReadOnly{&d, &th}}},
		// retired
		{"invalid: retired timestamp", LogState{Name: LogStateRetired}, logState{}},
		{"valid: retired", LogState{Name: LogStateRetired, EnteredAt: d}, logState{Retired: &logStateTimestamp{&d}}},
		// rejected
		{"invalid: rejected timestamp", LogState{Name: LogStateRejected}, logState{}},
		{"valid: rejected", LogState{Name: LogStateRejected, EnteredAt: d}, logState{Rejected: &logStateTimestamp{&d}}},
	} {
		got, err := table.input.internal()
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestLogStateReadOnlyTreeHeadParse(t *testing.T) {
	n := uint64(0)
	h := sha256.Sum256(nil)
	hs := h[:]
	hl := h[1:]
	for _, table := range []struct {
		desc     string
		input    logStateReadOnlyTreeHead
		wantSize uint64
		wantRoot [sha256.Size]byte
	}{
		{"invalid: nil size", logStateReadOnlyTreeHead{nil, &hs}, 0, [sha256.Size]byte{}},
		{"invalid: nil root", logStateReadOnlyTreeHead{&n, nil}, 0, [sha256.Size]byte{}},
		{"invalid: root len", logStateReadOnlyTreeHead{&n, &hl}, 0, [sha256.Size]byte{}},
		{"valid", logStateReadOnlyTreeHead{&n, &hs}, n, h},
	} {
		gotSize, gotRoot, err := table.input.parse()
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, gotSize, table.wantSize)
		tf.Equal(t, table.desc, gotRoot, table.wantRoot)
	}
}

func TestPreviousOperatorParse(t *testing.T) {
	n := ""
	d := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	for _, table := range []struct {
		desc  string
		input previousOperator
		want  PreviousOperator
	}{
		{"invalid: nil name", previousOperator{nil, &d}, PreviousOperator{}},
		{"invalid: nil end", previousOperator{&n, nil}, PreviousOperator{}},
		{"valid", previousOperator{&n, &d}, PreviousOperator{n, d}},
	} {
		var got PreviousOperator
		if tf.Error(t, table.desc, got.parse(table.input)) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestPreviousOperatorInternal(t *testing.T) {
	n := ""
	d := time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC)
	for _, table := range []struct {
		desc  string
		input PreviousOperator
		want  previousOperator
	}{
		{"invalid: check", PreviousOperator{n, time.Time{}}, previousOperator{}},
		{"valid", PreviousOperator{n, d}, previousOperator{&n, &d}},
	} {
		got, err := table.input.internal()
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}
