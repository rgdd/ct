// Package tls provides types and (de)serialization used by RFC 6962
package tls

import (
	"bytes"
	"encoding/binary"
	"fmt"
)

// HashAlgorithm is defined in RFC 5246:
// https://datatracker.ietf.org/doc/html/rfc5246.html#section-7.4.1.4.1
//
//	enum {
//	  sha256(4),
//	  (255)
//	} HashAlgorithm;
type HashAlgorithm uint8

// RFC 6962 only supports SHA256, see:
// https://www.rfc-editor.org/rfc/rfc6962#section-2.1
// https://www.rfc-editor.org/rfc/rfc6962#section-2.1.4
const (
	HashAlgorithmSHA256 = HashAlgorithm(4)
)

// SignatureAlgorithm is defined in RFC 5246:
// https://datatracker.ietf.org/doc/html/rfc5246.html#section-7.4.1.4.1
//
//	enum {
//	  rsa(1),
//	  ecdsa(3),
//	  (255)
//	} SignatureAlgorithm;
type SignatureAlgorithm uint8

// RFC 6962 only supports RSA and ECDSA, see:
// https://www.rfc-editor.org/rfc/rfc6962#section-2.1.4
const (
	SignatureAlgorithmRSA   = SignatureAlgorithm(1)
	SignatureAlgorithmECDSA = SignatureAlgorithm(3)
)

// DigitallySigned is defined in RFC 5246:
// https://datatracker.ietf.org/doc/html/rfc5246.html#section-4.7
// https://datatracker.ietf.org/doc/html/rfc5246.html#section-7.4.1.4.1
//
//	struct {
//	    HashAlgorithm hash;
//	    SignatureAlgorithm signature;
//	} SignatureAndHashAlgorithm;
//
//	struct {
//	    SignatureAndHashAlgorithm algorithm;
//	    opaque signature<0..2^16-1>;
//	} DigitallySigned;
type DigitallySigned struct {
	HashAlg HashAlgorithm
	SigAlg  SignatureAlgorithm
	Bytes   []byte
}

// Deserialize deserializes a digitally-signed element
func (ds *DigitallySigned) Deserialize(b []byte) error {
	if len(b) < 4 {
		return fmt.Errorf("signature: not enough bytes")
	}
	ds.HashAlg = HashAlgorithm(b[0])
	ds.SigAlg = SignatureAlgorithm(b[1])
	numBytes := binary.BigEndian.Uint16(b[2:4])
	ds.Bytes = b[4:]
	if int(numBytes) != len(ds.Bytes) {
		return fmt.Errorf("signature: invalid number of bytes %d (expected %d)", len(ds.Bytes), numBytes)
	}
	return ds.check()
}

// Serialize serializes a digitally-signed element
func (ds *DigitallySigned) Serialize() ([]byte, error) {
	buf := bytes.NewBuffer(nil)
	buf.Write([]byte{byte(ds.HashAlg)})
	buf.Write([]byte{byte(ds.SigAlg)})
	binary.Write(buf, binary.BigEndian, uint16(len(ds.Bytes)))
	buf.Write(ds.Bytes)
	return buf.Bytes(), ds.check()
}

func (ds *DigitallySigned) check() error {
	if ds.HashAlg != HashAlgorithmSHA256 {
		return fmt.Errorf("signature: hash algorithm %d is not supported", ds.HashAlg)
	}
	if ds.SigAlg != SignatureAlgorithmRSA && ds.SigAlg != SignatureAlgorithmECDSA {
		return fmt.Errorf("signature: signature algorithm %d is not supported", ds.SigAlg)
	}
	return nil
}

// TODO: (de)serialization helpers
