package tls

import (
	"bytes"
	"testing"

	"gitlab.torproject.org/rgdd/ct/internal/tf"
)

var (
	testDS      = DigitallySigned{HashAlgorithmSHA256, SignatureAlgorithmRSA, make([]byte, 258)}
	testDSBytes = bytes.Join([][]byte{
		[]byte{byte(HashAlgorithmSHA256)},
		[]byte{byte(SignatureAlgorithmRSA)},
		[]byte("\x01\x02"), // 256+2
		make([]byte, 258),
	}, nil)
)

func TestSignatureSerialize(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input DigitallySigned
		want  []byte
	}{
		{"invalid: check", DigitallySigned{}, nil},
		{"valid", testDS, testDSBytes},
	} {
		got, err := table.input.Serialize()
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestSignatureDeserialize(t *testing.T) {
	invalidOpts := append([]byte(nil), testDSBytes...)
	invalidOpts[0] = 0 // invalid hash algorithm
	for _, table := range []struct {
		desc  string
		input []byte
		want  DigitallySigned
	}{
		{"invalid: <4", nil, DigitallySigned{}},
		{"invalid: signature size", testDSBytes[:len(testDSBytes)-1], DigitallySigned{}},
		{"invalid: check", invalidOpts, DigitallySigned{}},
		{"valid", testDSBytes, testDS},
	} {
		var got DigitallySigned
		if tf.Error(t, table.desc, got.Deserialize(table.input)) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestSignatureCheck(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input DigitallySigned
	}{
		{"invalid: hash algorithm", DigitallySigned{HashAlgorithm(0), SignatureAlgorithmRSA, []byte("a")}},
		{"invalid: signature algorithm", DigitallySigned{HashAlgorithmSHA256, SignatureAlgorithm(0), []byte("a")}},
		{"valid: rsa", DigitallySigned{HashAlgorithmSHA256, SignatureAlgorithmRSA, []byte("a")}},
		{"valid: ecdsa", DigitallySigned{HashAlgorithmSHA256, SignatureAlgorithmECDSA, []byte("a")}},
	} {
		tf.Error(t, table.desc, table.input.check())
	}
}
