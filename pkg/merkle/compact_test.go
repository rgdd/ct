package merkle

import (
	"crypto/sha256"
	"fmt"
	"testing"

	"gitlab.torproject.org/rgdd/ct/internal/tf"
	"gitlab.torproject.org/rgdd/ct/pkg/merkle/testonly"
)

func TestCompactRange(t *testing.T) {
	for testCase, params := range testonly.CompactRangeParams {
		desc := fmt.Sprintf("valid: testonly.CompactRangeParams[%d]", testCase)
		tf.Equal(t, desc, compactRange(nodes(params.Index, hashLeaves(t, params.Data))), params.Nodes)

		//
		// Tampering with index doesn't affect the code path unless
		// there are two or more items.  In other words, the leaf hash
		// of the first item is the output with a single item.  It is
		// always invalid to increment/decrement index by one: it will
		// mess-up if the starting and/or ending node in the range does
		// (not) have a sibling.  Many indices give the same output
		// given the same input nodes, e.g., index=0 and index=4 with
		// the same four items is one example with the same output.
		//
		desc = fmt.Sprintf("invalid: testonly.CompactRangeParams[%d]: index: ", testCase)
		if len(params.Data) >= 2 {
			for _, index := range []uint64{params.Index + 1, params.Index - 1} {
				td := desc + fmt.Sprintf("%d", index)
				tf.Different(t, td, compactRange(nodes(index, hashLeaves(t, params.Data))), params.Nodes)
			}
		}

		//
		// It is always invalid to tamper with any data that is hashed.
		//
		desc = fmt.Sprintf("invalid: testonly.CompactRangeParams[%d]: data: ", testCase)
		var data [][][]byte
		for i := 0; i < len(params.Data); i++ {
			d := append([][]byte{}, params.Data...)
			d[i][0] ^= 1
			data = append(data, d)

			if i+1 != len(params.Data) {
				data = append(data, params.Data[i+1:])
			}
		}
		data = append(data, append(params.Data, []byte("extra")))
		for testCaseInner, d := range data {
			td := desc + fmt.Sprintf("test-case %d", testCaseInner)
			tf.Different(t, td, compactRange(nodes(params.Index, hashLeaves(t, d))), params.Nodes)
		}
	}
}

func TestTreeHeadFromRangeProof(t *testing.T) {
	for testCase, params := range testonly.InclusionParams {
		desc := fmt.Sprintf("valid: testonly.InclusionParams[%d]", testCase)
		got, err := TreeHeadFromRangeProof(hashLeaves(t, testonly.Range(params.Index, params.Size-1)), params.Index, params.Proof)
		if tf.Error(t, desc, err) {
			continue
		}
		if !tf.Equal(t, desc, got, params.Root) {
			continue
		}

		//
		// It is always invalid to add, remove, or tamper with data: we
		// will get at least one incorrect right-node subtree hash, and
		// in the case of removal we can also hit our constraint n > 0.
		//
		desc = fmt.Sprintf("invalid: testonly.InclusionParams[%d]: data: ", testCase)
		var data [][][]byte
		for i := uint64(0); i < params.Size-params.Index; i++ {
			d := testonly.Range(params.Index, params.Size-1)
			d[i][0] ^= 1
			data = append(data, d)
			data = append(data, testonly.Range(params.Index, params.Size-1)[i+1:])
		}
		data = append(data, testonly.Range(params.Index, params.Index+params.Size))
		data = append(data, testonly.Range(params.Index, 2*(params.Index+params.Size)))
		for testCaseInner, d := range data {
			got, err := TreeHeadFromRangeProof(hashLeaves(t, d), params.Index, params.Proof)
			if err != nil {
				continue // we're excepting error or root mismatch
			}
			td := desc + fmt.Sprintf("test-case %d", testCaseInner)
			tf.Different(t, td, got, params.Root)
		}

		//
		// Note that the computed right-node subtree hashes are always
		// correct for the passed index and data (which derives size).
		// The only error cases are therefore mismatched confirmations,
		// wrong number of proof hashes, and mismatched root hashes.
		//
		// There is also no point permutating index and proof for our
		// reference tree head: this is tested in TestVerifyInclusion.
		//
	}
}

func TestXOR(t *testing.T) {
	for _, table := range []struct {
		a, b   uint64
		output uint64
	}{
		{0, 0, 0},     // 0000 XOR 0000 -> 0000
		{0, 1, 1},     // 0000 XOR 0001 -> 0001
		{1, 1, 0},     // 0001 XOR 0001 -> 0000
		{15, 1, 14},   // 1111 XOR 0001 -> 1110
		{3, 12, 15},   // 0011 XOR 1100 -> 1111
		{123, 123, 0}, //  num XOR num  -> 0..0
	} {
		tf.Equal(t, fmt.Sprintf("valid: %d XOR %d", table.a, table.b), xor(table.a, table.b), table.output)
	}
}

func hashLeaves(t *testing.T, data [][]byte) (leaves [][sha256.Size]byte) {
	t.Helper()
	for _, d := range data {
		leaves = append(leaves, HashLeafNode(d))
	}
	return
}
