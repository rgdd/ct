package merkle

import (
	"crypto/sha256"
	"fmt"
	"math/rand"
	"testing"

	"gitlab.torproject.org/rgdd/ct/internal/tf"
	"gitlab.torproject.org/rgdd/ct/pkg/merkle/testonly"
)

func TestHashEmptyTree(t *testing.T) {
	tf.Equal(t, "valid: constant", HashEmptyTree(), testonly.TreeHeadEmpty)
}

func TestHashLeafNode(t *testing.T) {
	tf.Equal(t, "valid: leaf hash", HashLeafNode([]byte{'0'}), testonly.TH0T0)
	tf.Different(t, "valid: domain separation", HashLeafNode(nil), HashEmptyTree())
}

func TestHashInteriorNode(t *testing.T) {
	b1 := [sha256.Size]byte{}
	b2 := make([]byte, 2*sha256.Size)
	tf.Equal(t, "valid: interior hash", HashInteriorNode(testonly.TH0T0, testonly.TH1T1), testonly.TH0T1)
	tf.Different(t, "valid: domain separation", HashLeafNode(b2), HashInteriorNode(b1, b1))
}

func TestVerifyInclusion(t *testing.T) {
	for testCase, params := range testonly.InclusionParams {
		desc := fmt.Sprintf("valid: testonly.InclusionParams[%d]", testCase)
		if tf.Error(t, desc, VerifyInclusion(params.Data, params.Index, params.Size, params.Root, params.Proof)) {
			continue
		}

		//
		// Changing the data is always invalid.  We will get a root mismatch.
		//
		desc = fmt.Sprintf("invalid: testonly.InclusionParams[%d]: root mismatch", testCase)
		data := append([]byte{}, params.Data...)
		data[0] ^= 1
		tf.Error(t, desc, VerifyInclusion(data, params.Index, params.Size, params.Root, params.Proof))

		//
		// Changing the index is always invalid.  We will either be out
		// of range, or apply the proof's hashes in the wrong order.
		//
		desc = fmt.Sprintf("invalid: testonly.InclusionParams[%d]: index: ", testCase)
		for index := uint64(0); index < params.Size+1; index++ {
			if index == params.Index {
				continue
			}

			td := desc + fmt.Sprintf("%d", index)
			tf.Error(t, td, VerifyInclusion(params.Data, index, params.Size, params.Root, params.Proof))
		}

		//
		// Changing the size to a different _tree level_ is always
		// invalid.  It will mess-up the expected number of proof
		// hashes.  It is _not always invalid to just change the tree
		// size_.  For example, a valid inclusion proof for index 1 in a
		// tree of size 5 would also verify for identical inputs but
		// tree size 6.  This would obviously not be consistent, but the
		// inconsistency is however unrelated to the entry at index 1.
		//
		desc = fmt.Sprintf("invalid: testonly.InclusionParams[%d]: size: ", testCase)
		for _, size := range []uint64{params.Size / 2, params.Size * 2, params.Size*2 + randNum(t)} {
			td := desc + fmt.Sprintf("%d", size)
			tf.Error(t, td, VerifyInclusion(params.Data, params.Index, size, params.Root, params.Proof))
		}

		//
		// Changing the tree head is always invalid.  We will get a root
		// mismatch.
		//
		desc = fmt.Sprintf("invalid: testonly.InclusionParams[%d]: tree head", testCase)
		hash := sha256.Sum256([]byte("invalid hash"))
		tf.Error(t, desc, VerifyInclusion(params.Data, params.Index, params.Size, hash, params.Proof))

		//
		// It is always invalid to add or remove proof hashes: it
		// puts the tree level off by that number of added/removed
		// hashes.  It is always invalid to tamper with a single bit
		// somewhere in the proof: it creates a different tree head.
		//
		desc = fmt.Sprintf("invalid: testonly.InclusionParams[%d]: proof: ", testCase)
		var proofs [][][sha256.Size]byte
		for i := 0; i < len(params.Proof); i++ {
			proof := append([][sha256.Size]byte{}, params.Proof...)
			proof[i][0] ^= 1
			proofs = append(proofs, proof)
			proofs = append(proofs, params.Proof[i+1:])
		}
		proofs = append(proofs, append(params.Proof, hash))
		for testCaseInner, proof := range proofs {
			td := desc + fmt.Sprintf("test-case %d", testCaseInner)
			tf.Error(t, td, VerifyInclusion(params.Data, params.Index, params.Size, params.Root, proof))
		}
	}
}

func TestVerifyConsistency(t *testing.T) {
	for testCase, params := range testonly.ConsistencyParams {
		desc := fmt.Sprintf("valid: testonly.ConsistencyParams[%d]", testCase)
		if tf.Error(t, desc, VerifyConsistency(params.OldSize, params.NewSize, params.OldRoot, params.NewRoot, params.Proof)) {
			continue
		}

		//
		// Changing the size of the old tree is similiar to changing the
		// index in an inclusion proof.  (One way to view a consistency
		// proof is as an inclusion proof to a particular subtree.)  So,
		// we know all 0 < i < new_size for i != old_size are invalid.
		//
		desc = fmt.Sprintf("invalid: testonly.ConsistencyParams[%d]: old size: ", testCase)
		for oldSize := uint64(1); oldSize < params.NewSize; oldSize++ {
			if oldSize == params.OldSize {
				continue
			}

			td := desc + fmt.Sprintf("%d", oldSize)
			tf.Error(t, td, VerifyConsistency(oldSize, params.NewSize, params.OldRoot, params.NewRoot, params.Proof))
		}

		//
		// Changing the size of the new tree is similar to changing the
		// size of the tree when proving inclusion.  We intentionally
		// test with a tree level that is off by one, for the same
		// reason as outlined above in the inclusion proof test.  Just
		// skip cases where the new tree is consistent per definition.
		//
		desc = fmt.Sprintf("invalid: testonly.ConsistencyParams[%d]: new size: ", testCase)
		for _, newSize := range []uint64{params.NewSize / 2, params.NewSize * 2, params.NewSize*2 + randNum(t)} {
			if params.OldSize == 0 {
				break
			}

			td := desc + fmt.Sprintf("%d", newSize)
			tf.Error(t, td, VerifyConsistency(params.OldSize, newSize, params.OldRoot, params.NewRoot, params.Proof))
		}

		//
		// Changing tree heads is always invalid, except when the old
		// tree is empty and the new tree isn't.  We will get a root
		// mismatch.
		//
		desc = fmt.Sprintf("invalid: testonly.ConsistencyParams[%d]: old tree head", testCase)
		hash := sha256.Sum256([]byte("invalid hash"))
		tf.Error(t, desc, VerifyConsistency(params.OldSize, params.NewSize, hash, params.NewRoot, params.Proof))
		if params.OldSize != 0 {
			desc = fmt.Sprintf("invalid: testonly.ConsistencyParams[%d]: new tree head", testCase)
			tf.Error(t, desc, VerifyConsistency(params.OldSize, params.NewSize, params.OldRoot, hash, params.Proof))
		}

		//
		// It is always invalid to add or remove proof hashes; it puts
		// the tree size off by the number of added/removed hashes.  It
		// is always invalid to tamper with a single bit somewhere in
		// the proof: it creates a different tree head.
		//
		desc = fmt.Sprintf("invalid: testonly.ConsistencyParams[%d]: proof: ", testCase)
		var proofs [][][sha256.Size]byte
		for i := 0; i < len(params.Proof); i++ {
			proof := append([][sha256.Size]byte{}, params.Proof...)
			proof[i][0] ^= 1
			proofs = append(proofs, proof)
			proofs = append(proofs, params.Proof[i+1:])
		}
		proofs = append(proofs, append(params.Proof, hash))
		for testCaseInner, proof := range proofs {
			td := desc + fmt.Sprintf("test-case %d", testCaseInner)
			tf.Error(t, td, VerifyConsistency(params.OldSize, params.NewSize, params.OldRoot, params.NewRoot, proof))
		}
	}
}

func TestIsLSB(t *testing.T) {
	for i := 0; i < 1024; i += 2 {
		tf.Equal(t, fmt.Sprintf("isLSB(%d)", i), isLSB(uint64(i)), false)
		tf.Equal(t, fmt.Sprintf("isLSB(%d)", i), isLSB(uint64(i)+1), true)
	}
}

func TestIsPOW2(t *testing.T) {
	nums := []uint64{1, 2, 4, 8, 16, 32, 64, 128, 256, 9223372036854775808 /* 2^63 */}
	for _, num := range nums {
		tf.Equal(t, fmt.Sprintf("isPOW2(%d)", num), isPOW2(num), true)
	}
	nums = []uint64{3, 5, 6, 7, 9, 10, 11, 12, 13, 14, 15, 17, 18, 19, 20, 18446744073709551615 /* 2^64 - 1 */}
	for _, num := range nums {
		tf.Equal(t, fmt.Sprintf("isPOW2(%d)", num), isPOW2(num), false)
	}
}

func TestRshift(t *testing.T) {
	for _, table := range []struct {
		input  uint64
		output uint64
	}{
		{0, 0},   /* 0000 -> 000 */
		{1, 0},   /* 0001 -> 000 */
		{2, 1},   /* 0010 -> 001 */
		{3, 1},   /* 0011 -> 001 */
		{4, 2},   /* 0100 -> 010 */
		{5, 2},   /* 0101 -> 010 */
		{6, 3},   /* 0110 -> 011 */
		{7, 3},   /* 0111 -> 011 */
		{8, 4},   /* 1000 -> 100 */
		{9, 4},   /* 1001 -> 100 */
		{53, 26}, /* 110101 -> 11010 */
	} {
		tf.Equal(t, fmt.Sprintf("rshift(%d)", table.input), rshift(table.input), table.output)
	}
}

func randNum(t *testing.T) uint64 {
	t.Helper()
	return uint64(rand.Intn(1 << 30))
}
