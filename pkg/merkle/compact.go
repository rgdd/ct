package merkle

import (
	"crypto/sha256"
	"fmt"
)

// node represents a subtree at some level and a particular index
type node struct {
	index uint64
	hash  [sha256.Size]byte
}

// nodes returns a list of consecutive leaf nodes
func nodes(index uint64, leaves [][sha256.Size]byte) (n []node) {
	for i, leaf := range leaves {
		n = append(n, node{index + uint64(i), leaf})
	}
	return
}

// compactRange outputs the minimal number of fixed subtree hashes given a
// non-empty list of consecutive leaves that start from a non-zero index.  For a
// definition of this algorithm, see the end of ../../doc/tlog_algorithms.md.
func compactRange(nodes []node) [][sha256.Size]byte {
	// Step 1
	var hashes [][sha256.Size]byte

	// Step 2
	for len(nodes) > 1 {
		// Step 2a
		if xor(nodes[1].index, 1) != nodes[0].index {
			hashes = append(hashes, nodes[0].hash)
			nodes = nodes[1:]
		}

		// Step 2b; Step 2c;        Step 2c(iii)
		for i := 0; i < len(nodes); i++ {
			// Step 2c(i)
			if i+1 != len(nodes) {
				nodes[i].hash = HashInteriorNode(nodes[i].hash, nodes[i+1].hash)
				nodes = append(nodes[:i+1], nodes[i+2:]...)
			}

			// Step 2c(ii)
			nodes[i].index = rshift(nodes[i].index)
		}
	}

	// Step 3
	return append(hashes, nodes[0].hash)
}

// TreeHeadFromRangeProof computes a tree head at size n=len(leaves)+index if
// given a list of leaves at indices index,...,n-1 as well as an inclusion proof
// for the first leaf in the tree of size n.  This allows a verifier to check
// inclusion of one or more log entries with a single inclusion proof.
func TreeHeadFromRangeProof(leaves [][sha256.Size]byte, index uint64, proof [][sha256.Size]byte) (root [sha256.Size]byte, err error) {
	var cr [][sha256.Size]byte
	confirmHash := func(h [sha256.Size]byte) error {
		if h != cr[0] {
			return fmt.Errorf("aborted due incorrect right-node subtree hash")
		}
		cr = cr[1:]
		return nil
	}
	copyRoot := func(r [sha256.Size]byte) error {
		root = r
		return nil
	}

	if len(leaves) == 0 {
		return [sha256.Size]byte{}, fmt.Errorf("need at least one leaf to recompute tree head from proof")
	}
	if len(leaves) > 1 {
		cr = compactRange(nodes(index+1, leaves[1:]))
	}
	return root, inclusion(leaves[0], index, index+uint64(len(leaves)), proof, copyRoot, confirmHash)
}

func xor(a, b uint64) uint64 {
	return a ^ b
}
