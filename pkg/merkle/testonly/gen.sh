#!/bin/bash

#
# gen.sh
# Generates Merkle tree nodes by looking for referenced variables on the format
# "TH[0-9]+T[0-9]+" from all *.go files that are not the destination file.
#
# TH0T0 is assumed to refer to the node at index 0, with data "0".
# TH1T1 is assumed to refer to the node at index 1, with data "1".
# TH2T3 is assumed to refer to the parent of TH2T2 and TH3T3.
# TH0T3 is assumed to refer to the parent of TH0T1 and TH2T3.
# etc.
#
# In other words, the number of leaves in the generated Merkle tree is $hi-$lo+1
# for the input TH${lo}T${hi}.  The leaf data from left-to-right is $lo,...,$hi.
# Note that all leaf data are strings, e.g., the hex-encoding of "0" is 0x30.
#
# Usage:
#
#   $ ./gen.sh gen.go
#

set -eu
trap cleanup EXIT

tmp=$(mktemp)
function cleanup() {
	rm -f $tmp
}

function info() {
	echo "$(date +'%y-%m-%d %H:%M:%S %Z') [INFO] $@" >&2
}

function main() {
	dst=$1; shift
	src=$(ls *.go 2>/dev/null)

	info "output file is set to $dst"
	for file in $src; do
		if [[ $dst == $file ]]; then
			continue
		fi

		info "using input file $file"
		grep -o -E "TH[0-9]+T[0-9]+" $file >> $tmp
	done

	ths=$(cat $tmp | sort | uniq)
	info "found $(echo $ths | wc -w) Merkle tree heads to generate"
	info "additionally adding the empty tree head"

	echo "//go:generate $0 $dst" > $dst
	echo "package testonly" >> $dst
	echo "" >> $dst
	echo "import \"crypto/sha256\"" >> $dst
	echo "" >> $dst
	echo "var (" >> $dst

	./mth.py -n TreeHeadEmpty >> $dst
	for th in $ths; do
		lo=$(echo ${th:2} | cut -d'T' -f1)
		hi=$(echo ${th:2} | cut -d'T' -f2)
		mth $lo $hi >> $dst
	done

	echo ")" >> $dst
	info "done"

	go fmt $dst >/dev/null
}

function mth() {
	local i
	local dataOpts=""
	for i in `seq $1 $2`; do
		dataOpts="$dataOpts -d$i"
	done

	./mth.py -n "TH${1}T${2}" $dataOpts
}

main $@
