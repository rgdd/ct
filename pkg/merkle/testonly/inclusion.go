package testonly

import (
	"crypto/sha256"
	"fmt"
)

type InclusionParam struct {
	Data  []byte
	Index uint64
	Size  uint64
	Root  [sha256.Size]byte
	Proof [][sha256.Size]byte
}

func (p InclusionParam) String() string {
	return fmt.Sprintf("data %x at index %d in tree of size %d", p.Data, p.Index, p.Size)
}

var InclusionParams = []InclusionParam{
	// Size 1
	{[]byte{'0'}, 0, 1, TH0T0, nil},
	// Size 2
	{[]byte{'0'}, 0, 2, TH0T1, [][sha256.Size]byte{TH1T1}},
	{[]byte{'1'}, 1, 2, TH0T1, [][sha256.Size]byte{TH0T0}},
	// Size 3
	{[]byte{'0'}, 0, 3, TH0T2, [][sha256.Size]byte{TH1T1, TH2T2}},
	{[]byte{'1'}, 1, 3, TH0T2, [][sha256.Size]byte{TH0T0, TH2T2}},
	{[]byte{'2'}, 2, 3, TH0T2, [][sha256.Size]byte{TH0T1}},
	// Size 4
	{[]byte{'0'}, 0, 4, TH0T3, [][sha256.Size]byte{TH1T1, TH2T3}},
	{[]byte{'1'}, 1, 4, TH0T3, [][sha256.Size]byte{TH0T0, TH2T3}},
	{[]byte{'2'}, 2, 4, TH0T3, [][sha256.Size]byte{TH3T3, TH0T1}},
	{[]byte{'3'}, 3, 4, TH0T3, [][sha256.Size]byte{TH2T2, TH0T1}},
	// Size 5
	{[]byte{'0'}, 0, 5, TH0T4, [][sha256.Size]byte{TH1T1, TH2T3, TH4T4}},
	{[]byte{'1'}, 1, 5, TH0T4, [][sha256.Size]byte{TH0T0, TH2T3, TH4T4}},
	{[]byte{'2'}, 2, 5, TH0T4, [][sha256.Size]byte{TH3T3, TH0T1, TH4T4}},
	{[]byte{'3'}, 3, 5, TH0T4, [][sha256.Size]byte{TH2T2, TH0T1, TH4T4}},
	{[]byte{'4'}, 4, 5, TH0T4, [][sha256.Size]byte{TH0T3}},
	// Size 6
	{[]byte{'0'}, 0, 6, TH0T5, [][sha256.Size]byte{TH1T1, TH2T3, TH4T5}},
	{[]byte{'1'}, 1, 6, TH0T5, [][sha256.Size]byte{TH0T0, TH2T3, TH4T5}},
	{[]byte{'2'}, 2, 6, TH0T5, [][sha256.Size]byte{TH3T3, TH0T1, TH4T5}},
	{[]byte{'3'}, 3, 6, TH0T5, [][sha256.Size]byte{TH2T2, TH0T1, TH4T5}},
	{[]byte{'4'}, 4, 6, TH0T5, [][sha256.Size]byte{TH5T5, TH0T3}},
	{[]byte{'5'}, 5, 6, TH0T5, [][sha256.Size]byte{TH4T4, TH0T3}},
	// Size 7
	{[]byte{'0'}, 0, 7, TH0T6, [][sha256.Size]byte{TH1T1, TH2T3, TH4T6}},
	{[]byte{'1'}, 1, 7, TH0T6, [][sha256.Size]byte{TH0T0, TH2T3, TH4T6}},
	{[]byte{'2'}, 2, 7, TH0T6, [][sha256.Size]byte{TH3T3, TH0T1, TH4T6}},
	{[]byte{'3'}, 3, 7, TH0T6, [][sha256.Size]byte{TH2T2, TH0T1, TH4T6}},
	{[]byte{'4'}, 4, 7, TH0T6, [][sha256.Size]byte{TH5T5, TH6T6, TH0T3}},
	{[]byte{'5'}, 5, 7, TH0T6, [][sha256.Size]byte{TH4T4, TH6T6, TH0T3}},
	{[]byte{'6'}, 6, 7, TH0T6, [][sha256.Size]byte{TH4T5, TH0T3}},
	// Size 8
	{[]byte{'0'}, 0, 8, TH0T7, [][sha256.Size]byte{TH1T1, TH2T3, TH4T7}},
	{[]byte{'1'}, 1, 8, TH0T7, [][sha256.Size]byte{TH0T0, TH2T3, TH4T7}},
	{[]byte{'2'}, 2, 8, TH0T7, [][sha256.Size]byte{TH3T3, TH0T1, TH4T7}},
	{[]byte{'3'}, 3, 8, TH0T7, [][sha256.Size]byte{TH2T2, TH0T1, TH4T7}},
	{[]byte{'4'}, 4, 8, TH0T7, [][sha256.Size]byte{TH5T5, TH6T7, TH0T3}},
	{[]byte{'5'}, 5, 8, TH0T7, [][sha256.Size]byte{TH4T4, TH6T7, TH0T3}},
	{[]byte{'6'}, 6, 8, TH0T7, [][sha256.Size]byte{TH7T7, TH4T5, TH0T3}},
	{[]byte{'7'}, 7, 8, TH0T7, [][sha256.Size]byte{TH6T6, TH4T5, TH0T3}},
	// Size 9
	{[]byte{'0'}, 0, 9, TH0T8, [][sha256.Size]byte{TH1T1, TH2T3, TH4T7, TH8T8}},
	{[]byte{'1'}, 1, 9, TH0T8, [][sha256.Size]byte{TH0T0, TH2T3, TH4T7, TH8T8}},
	{[]byte{'2'}, 2, 9, TH0T8, [][sha256.Size]byte{TH3T3, TH0T1, TH4T7, TH8T8}},
	{[]byte{'3'}, 3, 9, TH0T8, [][sha256.Size]byte{TH2T2, TH0T1, TH4T7, TH8T8}},
	{[]byte{'4'}, 4, 9, TH0T8, [][sha256.Size]byte{TH5T5, TH6T7, TH0T3, TH8T8}},
	{[]byte{'5'}, 5, 9, TH0T8, [][sha256.Size]byte{TH4T4, TH6T7, TH0T3, TH8T8}},
	{[]byte{'6'}, 6, 9, TH0T8, [][sha256.Size]byte{TH7T7, TH4T5, TH0T3, TH8T8}},
	{[]byte{'7'}, 7, 9, TH0T8, [][sha256.Size]byte{TH6T6, TH4T5, TH0T3, TH8T8}},
	{[]byte{'8'}, 8, 9, TH0T8, [][sha256.Size]byte{TH0T7}},
	// Size 10
	{[]byte{'0'}, 0, 10, TH0T9, [][sha256.Size]byte{TH1T1, TH2T3, TH4T7, TH8T9}},
	{[]byte{'1'}, 1, 10, TH0T9, [][sha256.Size]byte{TH0T0, TH2T3, TH4T7, TH8T9}},
	{[]byte{'2'}, 2, 10, TH0T9, [][sha256.Size]byte{TH3T3, TH0T1, TH4T7, TH8T9}},
	{[]byte{'3'}, 3, 10, TH0T9, [][sha256.Size]byte{TH2T2, TH0T1, TH4T7, TH8T9}},
	{[]byte{'4'}, 4, 10, TH0T9, [][sha256.Size]byte{TH5T5, TH6T7, TH0T3, TH8T9}},
	{[]byte{'5'}, 5, 10, TH0T9, [][sha256.Size]byte{TH4T4, TH6T7, TH0T3, TH8T9}},
	{[]byte{'6'}, 6, 10, TH0T9, [][sha256.Size]byte{TH7T7, TH4T5, TH0T3, TH8T9}},
	{[]byte{'7'}, 7, 10, TH0T9, [][sha256.Size]byte{TH6T6, TH4T5, TH0T3, TH8T9}},
	{[]byte{'8'}, 8, 10, TH0T9, [][sha256.Size]byte{TH9T9, TH0T7}},
	{[]byte{'9'}, 9, 10, TH0T9, [][sha256.Size]byte{TH8T8, TH0T7}},
}
