#!/usr/bin/env python3

import sys
import argparse

from math import floor, log
from hashlib import sha256
from binascii import hexlify

def main(args):
    tree_hash = mth(args.data)

    s = f"\t{args.name} = [sha256.Size]byte{{"
    for i in range(len(tree_hash)):
        s += f"{tree_hash[i]},"

    output = f"{s[:-1]}}}"
    print(output)

def mth(entries):
    if entries is None:
        return empty_root_hash()

    assert(len(entries) != 0)
    if len(entries) == 1:
        return leaf_hash(entries[0])

    left, right = split(entries)
    return interior_hash(mth(left), mth(right))

def empty_root_hash():
    return sha256("".encode("utf-8")).digest()

def leaf_hash(leaf):
    buf = '\x00' + leaf
    return sha256(buf.encode("utf-8")).digest()

def interior_hash(left, right):
    buf = b'\x01' + left + right
    return sha256(buf).digest()

def split(entries):
    n = len(entries)
    if n % 2 == 0:
        n -= 1

    k = 2 ** floor(log(n, 2))
    return entries[:k], entries[k:]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="generates an RFC 6962 tree head for a list of data items",
    )
    parser.add_argument("-d", "--data",
        type=str, action="append",
        help="zero or more data items",
    )
    parser.add_argument("-n", "--name",
        type=str, required=True,
        help="Go variable name to use",
    )
    sys.exit(main(parser.parse_args()))
