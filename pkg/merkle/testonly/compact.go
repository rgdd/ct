package testonly

import (
	"crypto/sha256"
	"fmt"
)

type CompactRangeParam struct {
	Index uint64
	Data  [][]byte
	Nodes [][sha256.Size]byte
}

func (i CompactRangeParam) String() string {
	return fmt.Sprintf("index %d with %d data items", i.Index, len(i.Data))
}

var CompactRangeParams = []CompactRangeParam{
	// Compact range for an inclusion proof at index 0
	{1, Range(1, 1), [][sha256.Size]byte{TH1T1}},
	{1, Range(1, 2), [][sha256.Size]byte{TH1T1, TH2T2}},
	{1, Range(1, 3), [][sha256.Size]byte{TH1T1, TH2T3}},
	{1, Range(1, 4), [][sha256.Size]byte{TH1T1, TH2T3, TH4T4}},
	{1, Range(1, 5), [][sha256.Size]byte{TH1T1, TH2T3, TH4T5}},
	{1, Range(1, 6), [][sha256.Size]byte{TH1T1, TH2T3, TH4T6}},
	{1, Range(1, 7), [][sha256.Size]byte{TH1T1, TH2T3, TH4T7}},
	{1, Range(1, 8), [][sha256.Size]byte{TH1T1, TH2T3, TH4T7, TH8T8}},
	{1, Range(1, 9), [][sha256.Size]byte{TH1T1, TH2T3, TH4T7, TH8T9}},
	// Compact range for an inclusion proof at index 1
	{2, Range(2, 2), [][sha256.Size]byte{TH2T2}},
	{2, Range(2, 3), [][sha256.Size]byte{TH2T3}},
	{2, Range(2, 4), [][sha256.Size]byte{TH2T3, TH4T4}},
	{2, Range(2, 5), [][sha256.Size]byte{TH2T3, TH4T5}},
	{2, Range(2, 6), [][sha256.Size]byte{TH2T3, TH4T6}},
	{2, Range(2, 7), [][sha256.Size]byte{TH2T3, TH4T7}},
	{2, Range(2, 8), [][sha256.Size]byte{TH2T3, TH4T7, TH8T8}},
	{2, Range(2, 9), [][sha256.Size]byte{TH2T3, TH4T7, TH8T9}},
	{2, Range(2, 10), [][sha256.Size]byte{TH2T3, TH4T7, TH8T10}},
}

func Range(lo, hi uint64) (data [][]byte) {
	for i := lo; i <= hi; i++ {
		data = append(data, []byte(fmt.Sprintf("%d", i)))
	}
	return
}
