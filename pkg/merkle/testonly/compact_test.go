package testonly

import (
	"reflect"
	"testing"
)

func TestRange(t *testing.T) {
	for _, table := range []struct {
		lo, hi uint64
		want   [][]byte
	}{
		{1, 0, nil},
		{0, 0, [][]byte{[]byte("0")}},
		{0, 1, [][]byte{[]byte("0"), []byte("1")}},
		{1, 3, [][]byte{[]byte("1"), []byte("2"), []byte("3")}},
	} {
		r := Range(table.lo, table.hi)
		if got, want := r, table.want; !reflect.DeepEqual(got, want) {
			t.Errorf("[%d, %d]: got %v but wanted %v", table.lo, table.hi, got, want)
		}
	}
}
