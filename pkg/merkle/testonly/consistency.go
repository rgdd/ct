package testonly

import (
	"crypto/sha256"
	"fmt"
)

type ConsistencyParam struct {
	OldSize uint64
	NewSize uint64
	OldRoot [sha256.Size]byte
	NewRoot [sha256.Size]byte
	Proof   [][sha256.Size]byte
}

func (p ConsistencyParam) String() string {
	return fmt.Sprintf("old size %d to new size %d", p.OldSize, p.NewSize)
}

var ConsistencyParams = []ConsistencyParam{
	// Size 0
	{0, 0, TreeHeadEmpty, TreeHeadEmpty, nil},
	// Size 1
	{0, 1, TreeHeadEmpty, TH0T0, nil},
	{1, 1, TH0T0, TH0T0, nil},
	// Size 2
	{0, 2, TreeHeadEmpty, TH0T1, nil},
	{1, 2, TH0T0, TH0T1, [][sha256.Size]byte{TH1T1}},
	{2, 2, TH0T1, TH0T1, nil},
	// Size 3
	{0, 3, TreeHeadEmpty, TH0T2, nil},
	{1, 3, TH0T0, TH0T2, [][sha256.Size]byte{TH1T1, TH2T2}},
	{2, 3, TH0T1, TH0T2, [][sha256.Size]byte{TH2T2}},
	{3, 3, TH0T2, TH0T2, nil},
	// Size 4
	{0, 4, TreeHeadEmpty, TH0T3, nil},
	{1, 4, TH0T0, TH0T3, [][sha256.Size]byte{TH1T1, TH2T3}},
	{2, 4, TH0T1, TH0T3, [][sha256.Size]byte{TH2T3}},
	{3, 4, TH0T2, TH0T3, [][sha256.Size]byte{TH2T2, TH3T3, TH0T1}},
	{4, 4, TH0T3, TH0T3, nil},
	// Size 5
	{0, 5, TreeHeadEmpty, TH0T4, nil},
	{1, 5, TH0T0, TH0T4, [][sha256.Size]byte{TH1T1, TH2T3, TH4T4}},
	{2, 5, TH0T1, TH0T4, [][sha256.Size]byte{TH2T3, TH4T4}},
	{3, 5, TH0T2, TH0T4, [][sha256.Size]byte{TH2T2, TH3T3, TH0T1, TH4T4}},
	{4, 5, TH0T3, TH0T4, [][sha256.Size]byte{TH4T4}},
	{5, 5, TH0T4, TH0T4, nil},
	// Size 6
	{0, 6, TreeHeadEmpty, TH0T5, nil},
	{1, 6, TH0T0, TH0T5, [][sha256.Size]byte{TH1T1, TH2T3, TH4T5}},
	{2, 6, TH0T1, TH0T5, [][sha256.Size]byte{TH2T3, TH4T5}},
	{3, 6, TH0T2, TH0T5, [][sha256.Size]byte{TH2T2, TH3T3, TH0T1, TH4T5}},
	{4, 6, TH0T3, TH0T5, [][sha256.Size]byte{TH4T5}},
	{5, 6, TH0T4, TH0T5, [][sha256.Size]byte{TH4T4, TH5T5, TH0T3}},
	{6, 6, TH0T5, TH0T5, nil},
	// Size 7
	{0, 7, TreeHeadEmpty, TH0T6, nil},
	{1, 7, TH0T0, TH0T6, [][sha256.Size]byte{TH1T1, TH2T3, TH4T6}},
	{2, 7, TH0T1, TH0T6, [][sha256.Size]byte{TH2T3, TH4T6}},
	{3, 7, TH0T2, TH0T6, [][sha256.Size]byte{TH2T2, TH3T3, TH0T1, TH4T6}},
	{4, 7, TH0T3, TH0T6, [][sha256.Size]byte{TH4T6}},
	{5, 7, TH0T4, TH0T6, [][sha256.Size]byte{TH4T4, TH5T5, TH6T6, TH0T3}},
	{6, 7, TH0T5, TH0T6, [][sha256.Size]byte{TH4T5, TH6T6, TH0T3}},
	{7, 7, TH0T6, TH0T6, nil},
	// Size 8
	{0, 8, TreeHeadEmpty, TH0T7, nil},
	{1, 8, TH0T0, TH0T7, [][sha256.Size]byte{TH1T1, TH2T3, TH4T7}},
	{2, 8, TH0T1, TH0T7, [][sha256.Size]byte{TH2T3, TH4T7}},
	{3, 8, TH0T2, TH0T7, [][sha256.Size]byte{TH2T2, TH3T3, TH0T1, TH4T7}},
	{4, 8, TH0T3, TH0T7, [][sha256.Size]byte{TH4T7}},
	{5, 8, TH0T4, TH0T7, [][sha256.Size]byte{TH4T4, TH5T5, TH6T7, TH0T3}},
	{6, 8, TH0T5, TH0T7, [][sha256.Size]byte{TH4T5, TH6T7, TH0T3}},
	{7, 8, TH0T6, TH0T7, [][sha256.Size]byte{TH6T6, TH7T7, TH4T5, TH0T3}},
	{8, 8, TH0T7, TH0T7, nil},
	// Size 9
	{0, 9, TreeHeadEmpty, TH0T8, nil},
	{1, 9, TH0T0, TH0T8, [][sha256.Size]byte{TH1T1, TH2T3, TH4T7, TH8T8}},
	{2, 9, TH0T1, TH0T8, [][sha256.Size]byte{TH2T3, TH4T7, TH8T8}},
	{3, 9, TH0T2, TH0T8, [][sha256.Size]byte{TH2T2, TH3T3, TH0T1, TH4T7, TH8T8}},
	{4, 9, TH0T3, TH0T8, [][sha256.Size]byte{TH4T7, TH8T8}},
	{5, 9, TH0T4, TH0T8, [][sha256.Size]byte{TH4T4, TH5T5, TH6T7, TH0T3, TH8T8}},
	{6, 9, TH0T5, TH0T8, [][sha256.Size]byte{TH4T5, TH6T7, TH0T3, TH8T8}},
	{7, 9, TH0T6, TH0T8, [][sha256.Size]byte{TH6T6, TH7T7, TH4T5, TH0T3, TH8T8}},
	{8, 9, TH0T7, TH0T8, [][sha256.Size]byte{TH8T8}},
	{9, 9, TH0T8, TH0T8, nil},
	// Size 10
	{0, 10, TreeHeadEmpty, TH0T9, nil},
	{1, 10, TH0T0, TH0T9, [][sha256.Size]byte{TH1T1, TH2T3, TH4T7, TH8T9}},
	{2, 10, TH0T1, TH0T9, [][sha256.Size]byte{TH2T3, TH4T7, TH8T9}},
	{3, 10, TH0T2, TH0T9, [][sha256.Size]byte{TH2T2, TH3T3, TH0T1, TH4T7, TH8T9}},
	{4, 10, TH0T3, TH0T9, [][sha256.Size]byte{TH4T7, TH8T9}},
	{5, 10, TH0T4, TH0T9, [][sha256.Size]byte{TH4T4, TH5T5, TH6T7, TH0T3, TH8T9}},
	{6, 10, TH0T5, TH0T9, [][sha256.Size]byte{TH4T5, TH6T7, TH0T3, TH8T9}},
	{7, 10, TH0T6, TH0T9, [][sha256.Size]byte{TH6T6, TH7T7, TH4T5, TH0T3, TH8T9}},
	{8, 10, TH0T7, TH0T9, [][sha256.Size]byte{TH8T9}},
	{9, 10, TH0T8, TH0T9, [][sha256.Size]byte{TH8T8, TH9T9, TH0T7}},
	{10, 10, TH0T9, TH0T9, nil},
}
