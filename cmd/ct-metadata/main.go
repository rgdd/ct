// Package main provides a utility named ct-metadata.
//
// Install:
//
//	go install gitlab.torproject.org/rgdd/ct/cmd/ct-metadata@latest
//
// Usage:
//
//	$ ct-metadata -h
package main

import (
	"fmt"
	"os"

	"gitlab.torproject.org/rgdd/ct/cmd/ct-metadata/internal/get"
	"gitlab.torproject.org/rgdd/ct/cmd/ct-metadata/internal/lint"
	"gitlab.torproject.org/rgdd/ct/internal/ctflag"
)

const usage = `ct-metadata reads and verifies v3 metadata files

Usage:

  ct-metadata get   Download, verify, and output a metadata file
  ct-metadata lint  Try to parse a metadata file

Help:

  ct-metadata [-h] [--help]
  ct-metadata COMMAND [-h] [--help]

`

func main() {
	if ctflag.WantHelp(os.Args, 1) {
		fmt.Fprintf(os.Stderr, usage)
		os.Exit(1)
	}

	switch cmd, args := os.Args[1], os.Args[2:]; cmd {
	case "get":
		get.Main(args)
	case "lint":
		lint.Main(args)
	default:
		fmt.Fprintf(os.Stderr, "ct-metadata: unknown command %q\n\n", cmd)
		os.Exit(2)
	}
}
