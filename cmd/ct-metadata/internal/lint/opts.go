package lint

import (
	"flag"
	"fmt"
	"os"

	"gitlab.torproject.org/rgdd/ct/internal/ctflag"
)

const format = `Lint a local or remote metadata file

Usage:

  ct-metadata lint [-h] [--help]
  ct-metadata lint file -v VALUE [-n] [-d] [-s] [-t] [-D] [-T]
  ct-metadata lint url  -v VALUE [-n] [-d] [-s] [-t] [-D] [-T]

Options:

  -v, --value:        File name or URL to locate a metadata object (Default: "")
  -n, --name:         Require that operators have non-empty names (Default: false)
  -d, --description:  Require that logs have descriptions (Default: false)
  -s, --state:        Require that logs have states (Default: false)
  -t, --shard:        Require that logs have temporal shards (Default: false)
  -D, --dns:          Require that logs have DNS names (Default: false)
  -T, --type:         Require that logs have types (Default: false)

`

func usage() {
	fmt.Fprintf(os.Stderr, format)
}

type options struct {
	value        string
	oname        bool
	ldescription bool
	lstate       bool
	lshard       bool
	ldns         bool
	ltype        bool
}

func parse(args []string) (opts options, err error) {
	if ctflag.WantHelp(args, 0) {
		return opts, flag.ErrHelp
	}

	cmd := args[0]
	if cmd != "file" && cmd != "url" {
		return opts, fmt.Errorf("subcommand %q not supported", cmd)
	}

	fs := ctflag.NewFlagSet()
	ctflag.String(&fs, &opts.value, "value", "v", "")
	ctflag.Bool(&fs, &opts.oname, "name", "n", false)
	ctflag.Bool(&fs, &opts.ldescription, "description", "d", false)
	ctflag.Bool(&fs, &opts.lstate, "state", "s", false)
	ctflag.Bool(&fs, &opts.lshard, "shard", "t", false)
	ctflag.Bool(&fs, &opts.ldns, "dns", "D", false)
	ctflag.Bool(&fs, &opts.ltype, "type", "T", false)

	args = args[1:]
	return opts, ctflag.Parse(fs, args)
}
