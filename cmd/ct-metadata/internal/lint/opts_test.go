package lint

import (
	"strings"
	"testing"

	"gitlab.torproject.org/rgdd/ct/internal/tf"
)

func TestUsage(t *testing.T) {
	tf.Equal(t, "santity checks", len(tf.MapOptions(t, format)), 7)
}

func TestParse(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  options
	}{
		{"invalid: help", "help", options{}},
		{"invalid: subcommand", "foo -v /tmp/abc", options{}},
		{"invalid: unknown option", "file -v /tmp/abc -X foo", options{}},
		{"valid: default (url)", "url -v http://example.org", options{value: "http://example.org"}},
		{"valid: default (file)", "file -v /tmp/abc", options{value: "/tmp/abc"}},
		{"valid: require name", "file -v /tmp/abc -n", options{value: "/tmp/abc", oname: true}},
		{"valid: require description", "file -v /tmp/abc -d", options{value: "/tmp/abc", ldescription: true}},
		{"valid: require state", "file -v /tmp/abc -s", options{value: "/tmp/abc", lstate: true}},
		{"valid: require shard", "file -v /tmp/abc -t", options{value: "/tmp/abc", lshard: true}},
		{"valid: require dns", "file -v /tmp/abc -D", options{value: "/tmp/abc", ldns: true}},
		{"valid: require type", "file -v /tmp/abc -T", options{value: "/tmp/abc", ltype: true}},
	} {
		gotShort, err := parse(strings.Split(table.input, " "))
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, gotShort, table.want)

		gotLong, err := parse(strings.Split(tf.ReplaceShortWithLong(t, format, table.input), " "))
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, gotShort, gotLong)
	}
}
