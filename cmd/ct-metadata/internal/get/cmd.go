package get

import (
	"context"
	"fmt"
	"os"

	"gitlab.torproject.org/rgdd/ct/internal/ctio"
	"gitlab.torproject.org/rgdd/ct/pkg/metadata"
)

func cmd(opts options) (err error) {
	sopts := metadata.HTTPSourceOptions{Name: opts.name}
	if opts.name == "" {
		if sopts.PublicKey, err = ctio.PublicKeyFromPEMFile(opts.keyFile); err != nil {
			return
		}
		sopts.MetadataURL = opts.metadataURL
		sopts.SignatureURL = opts.signatureURL
	}
	s := metadata.NewHTTPSource(sopts)
	m, _, _, err := s.Load(context.Background())
	if err != nil {
		return
	}

	fmt.Fprintf(os.Stdout, "%s\n", string(m))
	return
}
