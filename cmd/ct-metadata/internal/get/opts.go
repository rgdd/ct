package get

import (
	"flag"
	"fmt"
	"os"

	"gitlab.torproject.org/rgdd/ct/internal/ctflag"
)

const format = `Get a metadata file with a valid signature

Usage:

  ct-metadata get [-h] [--help]
  ct-metadata get custom -k KEY_FILE -m METADATA_URL -s SIGNATURE_URL
  ct-metadata get source -n NAME

Options:

  -k, --key-file:      Key file for signature verification (Default: "")
  -m, --metadata-url:  URL where the metadata file is hosted (Default: "")
  -s, --signature-url: URL where the metadata signature is hosted (Default: "")
  -n, --name:          Name of a source with a hardcoded key, metadata URL, and
                       signature URL (Default: "", Available: "google")

The public key is a PEM-encoded ASN.1 SubjectPublicKeyInfo in DER format.  The
signature is either made with Ed25519 (no -ctx and -ph) or RSASSA PKCS#1 v1.5.

`

func usage() {
	fmt.Fprintf(os.Stderr, format)
}

var sources = []string{
	"google",
}

type options struct {
	// custom
	keyFile      string
	metadataURL  string
	signatureURL string

	// source
	name string
}

func parse(args []string) (opts options, err error) {
	if ctflag.WantHelp(args, 0) {
		return opts, flag.ErrHelp
	}

	fs := ctflag.NewFlagSet()
	cmd := args[0]
	switch cmd {
	case "custom":
		ctflag.String(&fs, &opts.keyFile, "key-file", "k", "")
		ctflag.String(&fs, &opts.metadataURL, "metadata-url", "m", "")
		ctflag.String(&fs, &opts.signatureURL, "signature-url", "s", "")
	case "source":
		ctflag.String(&fs, &opts.name, "name", "n", "")
	default:
		return opts, fmt.Errorf("subcommand %q not supported", cmd)
	}

	args = args[1:]
	if err := ctflag.Parse(fs, args); err != nil {
		return opts, err
	}

	switch cmd {
	case "custom":
		if len(opts.keyFile) == 0 {
			return opts, fmt.Errorf("key file must be non-empty")
		}
		if len(opts.metadataURL) == 0 {
			return opts, fmt.Errorf("metadata url must be non-empty")
		}
		if len(opts.signatureURL) == 0 {
			return opts, fmt.Errorf("signature url must be non-empty")
		}
	case "source":
		ok := false
		for _, source := range sources {
			if opts.name == source {
				ok = true
			}
		}
		if !ok {
			return opts, fmt.Errorf("name %q is not supported", opts.name)
		}
	}
	return opts, nil
}
