// Package get provides an entrypoint to a subcommand
package get

import (
	"errors"
	"flag"
	"fmt"
	"os"
)

// Main runs a subcommand named args[0] with options args[1:]
func Main(args []string) {
	opts, err := parse(args)
	if err != nil {
		if errors.Is(err, flag.ErrHelp) {
			usage()
			os.Exit(3)
		}

		fmt.Fprintf(os.Stderr, "ERROR: %v\n\n", err)
		os.Exit(4)
	}
	if err := cmd(opts); err != nil {
		fmt.Fprintf(os.Stderr, "ERROR: %v\n\n", err)
		os.Exit(5)
	}
}
