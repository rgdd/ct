package get

import (
	"strings"
	"testing"

	"gitlab.torproject.org/rgdd/ct/internal/tf"
)

func TestUsage(t *testing.T) {
	tf.Equal(t, "santity checks", len(tf.MapOptions(t, format)), 4)
}

func TestParse(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  options
	}{
		{"invalid: help", "help", options{}},
		{"invalid: subcommand", "foo -n google", options{}},
		// custom
		{"invalid: no key file", "custom -m http://foo.org -s http://bar.org", options{}},
		{"invalid: no meta url ", "custom -k /tmp/abc -s http://bar.org", options{}},
		{"invalid: no sig url", "custom -k /tmp/abc -m http://foo.org", options{}},
		{
			desc:  "valid: custom",
			input: "custom -k /tmp/abc -m http://foo.org -s http://bar.org",
			want:  options{keyFile: "/tmp/abc", metadataURL: "http://foo.org", signatureURL: "http://bar.org"},
		},
		// source
		{"invalid: unknown option", "source -n google -X foo", options{}},
		{"invalid: unknown name", "source -n foo", options{}},
		{"valid: source", "source -n google", options{name: "google"}},
	} {
		gotShort, err := parse(strings.Split(table.input, " "))
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, gotShort, table.want)

		gotLong, err := parse(strings.Split(tf.ReplaceShortWithLong(t, format, table.input), " "))
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, gotShort, gotLong)
	}
}
