package ctflag

import (
	"flag"
	"fmt"
)

// NewFlagSet outputs a new flag set that continues on errors without standard
// library prints (so that the application gets full control of error handling)
func NewFlagSet() flag.FlagSet {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	fs.SetOutput(discard{})
	return *fs
}

// Parse tries to parse command-line options into a flag set.  Trailing
// arguments are not permitted, i.e., all options must be parseable flags.
func Parse(fs flag.FlagSet, args []string) error {
	if err := fs.Parse(args); err != nil {
		return err
	}
	if len(fs.Args()) != 0 {
		return fmt.Errorf("trailing arguments: %v", fs.Args())
	}
	return nil
}

// WantHelp outputs true if the index i is out-of-range, or if the argument at
// index i starts with "help" or "-"
func WantHelp(args []string, i int) bool {
	if i+1 > len(args) {
		return true
	}
	if args[i] == "help" {
		return true
	}
	return len(args[i]) == 0 || args[i][0] == '-'
}

// String adds a new string option to a flag set
func String(fs *flag.FlagSet, opt *string, short, long, value string) {
	fs.StringVar(opt, short, value, "")
	fs.StringVar(opt, long, value, "")
}

// Bool adds a new bool option to a flag set
func Bool(fs *flag.FlagSet, opt *bool, short, long string, value bool) {
	fs.BoolVar(opt, short, value, "")
	fs.BoolVar(opt, long, value, "")
}

type discard struct{}

func (d discard) Write(_ []byte) (int, error) {
	return 0, nil
}
