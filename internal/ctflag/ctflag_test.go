package ctflag

import (
	"strings"
	"testing"

	"gitlab.torproject.org/rgdd/ct/internal/tf"
)

func TestParse(t *testing.T) {
	type options struct {
		Name   string
		Enable bool
	}

	for _, table := range []struct {
		desc  string
		input []string
		want  options
	}{
		{"invalid: trailing args (1/2)", []string{"-n", "alice", "-e", "foo"}, options{}},
		{"invalid: trailing args (2/2)", []string{"foo", "-n", "alice", "-e"}, options{}},
		{"valid: default", []string{}, options{"alice", true}},
		// Set string
		{"invalid: no separator (string)", []string{"-nbob"}, options{}},
		{"valid: set string (1/2)", []string{"-n", "bob"}, options{"bob", true}},
		{"valid: set string (2/2)", []string{"-n=bob"}, options{"bob", true}},
		// Set bool
		{"invalid: no separator (bool)", []string{"-efalse"}, options{}},
		{"invalid: caps (bool)", []string{"-e=TRUe"}, options{"alice", true}},
		{"valid: set bool (1/6)", []string{"-e=false"}, options{"alice", false}},
		{"valid: set bool (2/6)", []string{"-e=False"}, options{"alice", false}},
		{"valid: set bool (3/6)", []string{"-e=FALSE"}, options{"alice", false}},
		{"valid: set bool (4/6)", []string{"-e=true"}, options{"alice", true}},
		{"valid: set bool (5/6)", []string{"-e=True"}, options{"alice", true}},
		{"valid: set bool (6/6)", []string{"-e=TRUE"}, options{"alice", true}},
		// Typical examples
		{"valid: short", strings.Split("-n alice -e", " "), options{"alice", true}},
		{"valid: long (-)", strings.Split("-name alice -enable", " "), options{"alice", true}},
		{"valid: long (--)", strings.Split("--name alice --enable", " "), options{"alice", true}},
	} {

		fs := NewFlagSet()
		got := options{}
		String(&fs, &got.Name, "n", "name", "alice")
		Bool(&fs, &got.Enable, "e", "enable", true)

		if tf.Error(t, table.desc, Parse(fs, table.input)) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}

func TestWantHelp(t *testing.T) {
	for _, table := range []struct {
		desc  string
		args  []string
		index int
		want  bool
	}{
		{"yes: out of range index 0", []string{}, 0, true},
		{"yes: out of range index 1", []string{"/tmp/test"}, 1, true},
		{"yes: help requested index 0", []string{"help", "foo"}, 0, true},
		{"yes: help requested index 1", []string{"/tmp/test", "help"}, 1, true},
		{"yes: starts with -", []string{"/tmp/test", "-h"}, 1, true},
		{"yes: starts with --", []string{"/tmp/test", "--h"}, 1, true},
		{"no", []string{"/tmp/test", "command", "-n", "alice"}, 1, false},
	} {
		tf.Equal(t, table.desc, WantHelp(table.args, table.index), table.want)
	}
}
