package ctio

import "time"

const (
	// HTTPTimeout is a timeout for HTTP requests
	HTTPTimeout = 15 * time.Second

	// HTTPUserAgent is a user-agent string that identifies this library
	HTTPUserAgent = "rgdd.se/ct"
)
