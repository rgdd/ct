package ctio

import (
	"crypto"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"os"
)

func BytesFromPEM(b []byte, pemType string) ([]byte, error) {
	block, rest := pem.Decode(b)
	if block == nil {
		return nil, fmt.Errorf("no PEM block")
	}
	if block.Type != pemType {
		return nil, fmt.Errorf("invalid PEM type %s", block.Type)
	}
	if len(rest) != 0 {
		return nil, fmt.Errorf("%d trailing PEM bytes", len(rest))
	}
	return block.Bytes, nil
}

func PublicKeyFromPEM(b []byte) (crypto.PublicKey, error) {
	der, err := BytesFromPEM(b, "PUBLIC KEY")
	if err != nil {
		return struct{}{}, err
	}
	return x509.ParsePKIXPublicKey(der)
}

func PublicKeyFromPEMFile(fileName string) (crypto.PublicKey, error) {
	b, err := os.ReadFile(fileName)
	if err != nil {
		return struct{}{}, err
	}
	return PublicKeyFromPEM(b)
}
