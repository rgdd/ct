package ctio

import (
	"fmt"
	"testing"

	"gitlab.torproject.org/rgdd/ct/internal/tf"
)

func TestBytesFromPEM(t *testing.T) {
	for _, table := range []struct {
		desc  string
		input string
		want  []byte
	}{
		{"invalid: no block", fmt.Sprintf("Zm9v"), nil},
		{"invalid: wrong type", fmt.Sprintf("-----BEGIN bar-----\nZm9v\n-----END bar-----\n"), nil},
		{"invalid: extra bytes", fmt.Sprintf("-----BEGIN foo-----\nZm9v\n-----END foo-----\nZm9v"), []byte("foo")},
		{"valid", fmt.Sprintf("-----BEGIN foo-----\nZm9v\n-----END foo-----\n"), []byte("foo")},
	} {
		got, err := BytesFromPEM([]byte(table.input), "foo")
		if tf.Error(t, table.desc, err) {
			continue
		}
		tf.Equal(t, table.desc, got, table.want)
	}
}
