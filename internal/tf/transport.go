package tf

import (
	"bytes"
	"fmt"
	"net/http"
	"testing"
)

// Transport implements the http.RoundTripper interface for use in tests
type Transport struct {
	t      *testing.T
	params *transportParams
}

type transportParams struct {
	message [][]byte // RoundTrip tries to consume message[0]
	status  []int    // RoundTrip tries to consume status[0]
}

// NewTransport outputs a new test transport
func NewTransport(t *testing.T, message [][]byte, status []int) Transport {
	t.Helper()
	return Transport{t, &transportParams{message, status}}
}

// RoundTrip tries to create a test response to an HTTP request.  An error is
// created if a test response is not available.
func (t Transport) RoundTrip(_ *http.Request) (*http.Response, error) {
	t.t.Helper()
	if t.params == nil {
		t.t.Fatalf("missing transport data")
	}

	if len(t.params.message) == 0 {
		return nil, fmt.Errorf("unexpected call to RoundTrip (missing message[0])")
	}
	if len(t.params.status) == 0 {
		return nil, fmt.Errorf("unexpected call to RoundTrip (missing status[0])")
	}
	rsp := http.Response{
		Body:       newBuffer(t.t, t.params.message[0]),
		StatusCode: t.params.status[0],
	}
	t.params.message = t.params.message[1:]
	t.params.status = t.params.status[1:]

	return &rsp, nil
}

// Check reports errors if any items are left to be consumed
func (t Transport) Check() {
	t.t.Helper()
	if t.params == nil {
		t.t.Fatalf("missing transport data")
	}
	if n := len(t.params.message); n != 0 {
		t.t.Errorf("missing %d calls to RoundTrip (message)", n)
	}
	if n := len(t.params.status); n != 0 {
		t.t.Errorf("missing %d calls to RoundTrip (status)", n)
	}
}

// buffer implements the ReadCloser interface
type buffer struct {
	t *testing.T
	*bytes.Buffer
}

func newBuffer(t *testing.T, b []byte) buffer {
	t.Helper()
	return buffer{t, bytes.NewBuffer(b)}
}

func (b buffer) Close() error {
	b.t.Helper()
	return nil
}
