package tf

import (
	"fmt"
	"strings"
	"testing"
)

// MapOptions extracts options from lines in a usage message that starts with:
//
//	<space><space>-<character>,<space>--<characters>:
func MapOptions(t *testing.T, usage string) map[byte]string {
	t.Helper()
	m := make(map[byte]string)
	for _, line := range strings.Split(usage, "\n") {
		if len(line) < 10 {
			continue
		}
		if line[:3] != "  -" {
			continue
		}
		if line[4:8] != ", --" {
			continue
		}
		i := strings.Index(line[8:], ":")
		if i == -1 {
			continue
		}
		short := line[3]
		long := line[8 : 8+i]
		m[short] = long
	}
	return m
}

// ReplaceShortWithLong transforms a command-line string with short options to
// an equivalent one with long options, substituting from a usage message.  The
// usage message must be formatted as described in MapOptions, see above.
//
// Be warned: substitution is not be perfect, but it should be good enough to
// not cause headaches while catching most inconsistencies between usage
// messages and the parameters which are passed to a command's flag set.
func ReplaceShortWithLong(t *testing.T, usage, cmdLine string) (ret string) {
	t.Helper()
	m := MapOptions(t, usage)
	curr := 0
	for {
		rest := cmdLine[curr:]
		match := strings.Index(rest, "-")
		if match == -1 {
			break // no more short options
		}
		if match+1 >= len(rest) {
			break // ended with "-" (so not an option)
		}
		short := rest[match+1]

		// Only substitute if this short option is at the end of a line,
		// or if it is followed by an appropriate separator character.
		// For example, this avoids substitutions in test data like "-u
		// http://my-test.example.org/" and "-w test-file".
		if match+2 >= len(rest) || rest[match+2] == ' ' || rest[match+2] == '=' {
			long, ok := m[short]
			if !ok {
				t.Fatalf("option -%c is not in usage message", short)
			}

			ret += rest[:match]
			ret += fmt.Sprintf("--%s", long)
			curr += match + 2
		}
	}
	ret += cmdLine[curr:]
	return ret
}
