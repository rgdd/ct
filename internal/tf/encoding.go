package tf

import (
	"encoding/base64"
	"testing"
)

// B64 outputs base64-encodes a byte slice
func B64(t *testing.T, b []byte) string {
	t.Helper()
	return base64.StdEncoding.EncodeToString(b)
}

// B64Dec base64-decodes a string
func B64Dec(t *testing.T, s string) []byte {
	t.Helper()
	b, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		t.Fatalf("decode %q: %v", s, err)
	}
	return b
}
