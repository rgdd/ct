// Package tf provides common test functions
package tf

import (
	"reflect"
	"testing"
)

// Error checks if an error from a previous operation was expected by checking
// if desc starts with the string "valid".  An error is printed if the value of
// err is unexpected.  True is returned if err is non-nil or wrongly nil.
func Error(t *testing.T, desc string, err error) bool {
	t.Helper()
	if got, want := err != nil, len(desc) < 5 || desc[:5] != "valid"; got != want {
		if err != nil {
			t.Errorf("test case %q: expected no error but got: %v", desc, err)
		} else {
			t.Errorf("test case %q: expected error but got none", desc)
		}
		return true // always skip after unexpected result
	}
	return err != nil // always skip expected error
}

// Equal checks if a and b are deeply equal, see reflect.DeepEqual().  An error
// is printed if a and b are not equal.  The output is then set to false.
func Equal(t *testing.T, desc string, got, want any) bool {
	t.Helper()
	if reflect.DeepEqual(got, want) {
		return true
	}
	t.Errorf("test case %q: expected equality\ngot:  %v\nwant: %v", desc, got, want)
	return false
}

// Different checks if a and b are not deeply equal, see reflect.DeepEqual().
// An error is printed if a and b are equal.  The output is then set to false.
func Different(t *testing.T, desc string, got, want any) bool {
	t.Helper()
	if !reflect.DeepEqual(got, want) {
		return true
	}
	t.Errorf("test case %q: expected non-equality\ngot:  %v", desc, got)
	return false
}
