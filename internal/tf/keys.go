package tf

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"testing"
)

// ECDSA outputs a new ECDSA public-key pair
func ECDSA(t *testing.T) (pubID [sha256.Size]byte, pubDER []byte, pub crypto.PublicKey, priv *ecdsa.PrivateKey) {
	t.Helper()
	var err error

	if priv, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader); err != nil {
		t.Fatal(err)
	}
	pub = priv.Public()
	if pubDER, err = x509.MarshalPKIXPublicKey(pub); err != nil {
		t.Fatal(err)
	}
	pubID = sha256.Sum256(pubDER)

	return
}

// RSA outputs a new RSA public-key pair
func RSA(t *testing.T, bits int) (pubID [sha256.Size]byte, pubDER []byte, pub crypto.PublicKey, priv *rsa.PrivateKey) {
	t.Helper()
	var err error

	if priv, err = rsa.GenerateKey(rand.Reader, bits); err != nil {
		t.Fatal(err)
	}
	pub = priv.Public()
	if pubDER, err = x509.MarshalPKIXPublicKey(pub); err != nil {
		t.Fatal(err)
	}
	pubID = sha256.Sum256(pubDER)

	return
}

// Ed25519 outputs a new Ed25519 public-key pair
func Ed25519(t *testing.T) (pubID [sha256.Size]byte, pubDER []byte, pub crypto.PublicKey, priv *ed25519.PrivateKey) {
	t.Helper()

	vk, sk, err := ed25519.GenerateKey(rand.Reader)
	if err != nil {
		t.Fatal(err)
	}
	priv = &sk
	pub = vk
	if pubDER, err = x509.MarshalPKIXPublicKey(pub); err != nil {
		t.Fatal(err)
	}
	pubID = sha256.Sum256(pubDER)

	return
}

// RSASign signs a message using RSA-PKCS1-V1_5 with SHA256
func RSASign(t *testing.T, priv *rsa.PrivateKey, msg []byte) (sig []byte) {
	t.Helper()
	var err error

	h := sha256.Sum256(msg)
	if sig, err = rsa.SignPKCS1v15(nil, priv, crypto.SHA256, h[:]); err != nil {
		t.Fatalf("%v", err)
	}

	return
}

// Ed25519Sign signs a message using Ed25519
func Ed25519Sign(t *testing.T, priv *ed25519.PrivateKey, msg []byte) []byte {
	t.Helper()
	return ed25519.Sign(*priv, msg)
}
