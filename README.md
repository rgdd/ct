# Certificate Transparency

A simple client-side standard library implementation of [Certificate
Transparency][] (CT) in Go.

[Certificate Transparency]: https://certificate.transparency.dev/

## Status

Work in progress.

## Goals

  - No external dependencies or forked libraries in [pkg/](./pkg)
  - Easy-to-use abstractions with strict parsing and verification by default
  - Reviewable from start to finish in less than a day if you know CT and Go

## Non-goals

  - More functionality than what an [RFC 6962][] client needs in practise
  - Relaxed parsing rules to work with [malformed certificates][]

[RFC 6962]: https://datatracker.ietf.org/doc/html/rfc6962/
[malformed certificates]: https://github.com/google/certificate-transparency-go/blob/v1.1.3/README.md#repository-structure/

## Utilities

To be added.

## Contact

  - IRC: #certificate-transparency @ OFTC.net
  - Anywhere else you see fit

## Licence

BSD 2-Clause License
